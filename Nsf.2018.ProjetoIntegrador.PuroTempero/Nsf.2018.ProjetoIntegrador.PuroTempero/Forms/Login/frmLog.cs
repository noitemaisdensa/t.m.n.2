﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login
{
    public partial class frmLog : Form
    {
        public frmLog()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Tem certeza que deseja finalizar o programa?",
                                                  "Puro Tempero",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private VIEW_AcessoDTO CarregarControles (VIEW_AcessoDTO dto)
        {
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;

            return dto;
        }
        private VIEW_AcessoDTO VerificarUser (VIEW_AcessoDTO user)
        {
            if (user != null)
            {
                UserSession.Logado = user;

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            else
            {
                MessageBox.Show("Credênciais inválidas!!",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error);
            }
            return user;
        }

        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            VIEW_AcessoDTO dto = new VIEW_AcessoDTO();
            CarregarControles(dto);

            VIEW_Acesso db = new VIEW_Acesso();
            VIEW_AcessoDTO user = db.Logar(dto);
            VerificarUser(user);
        }
        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                VIEW_AcessoDTO dto = new VIEW_AcessoDTO();
                CarregarControles(dto);

                VIEW_Acesso db = new VIEW_Acesso();
                VIEW_AcessoDTO user = db.Logar(dto);
                VerificarUser(user);
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "Usuário")
            {
                txtUsuario.Text = "";
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                txtUsuario.Text = "Usuário";
                txtUsuario.ForeColor = Color.Silver;
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            if (txtSenha.Text == "Senha")
            {
                txtSenha.Text = "";
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            if (txtSenha.Text == "")
            {
                txtSenha.Text = "Senha";
                txtSenha.ForeColor = Color.Silver;
            }
        }

    }
}
