﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Módulo_de_Acesso.Funcionário;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    public partial class frmAlterarSenha : Form
    {
        public object Business_Funcionario { get; private set; }

        public frmAlterarSenha ()
        {
            InitializeComponent();
        }

        private void gpbLogin_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);


                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmLog tela = new frmLog();
            tela.Show();
            this.Hide();
        }

        public void ValidarSenha()
        {
            //if (txtNovaSenha.Text != txtRepitaSenha.Text)
            {
                //Por enquanto, vamos trabalhar com a MessageBox
                MessageBox.Show("Senhas incorretas!!",
                                "Sigma",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }
        }

        private void btnTrocar_Click(object sender, EventArgs e)
        {
            //DTO_Funcionario dto = new DTO_Funcionario();
            //dto.Usuario = txtUsuario.Text;
            //
            //Database_Funcionario db = new Database_Funcionario();
            //bool verdadeiro = db.VerificarSenha(txtSenhaAtual.Text);
            //if (verdadeiro == true)
            //{
            //    ValidarSenha();
            //    dto.Senha = txtNovaSenha.Text;
            //}
            //
            //Business_Funcionario business = new Business_Funcionario();
            //business.Alterar(dto);

            //Trocando as propriedades da label
            //lblMensagem.ForeColor = Color.White;
            //lblMensagem.BackColor = Color.ForestGreen;

            //Mostrando o valor para o usuário.
            //lblMensagem.Text = "Senha Alterada com Sucesso!";


        }
    }
}
