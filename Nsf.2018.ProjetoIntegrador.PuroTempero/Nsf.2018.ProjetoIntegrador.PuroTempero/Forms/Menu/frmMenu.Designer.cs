﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBaterB = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastroCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastroFornecedor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastroFuncionario = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultaCc = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultaCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarFuncionario = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarFornecedor = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoVendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarPedidoCompra = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarPedidoCompra = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarProdutoCompra = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarProdutoCompra = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarPV = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarPedidoVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarPedidoVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCadastrarProdutoVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultarProdutoVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFolha = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFluxo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLogoff = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlConteudo = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFechar = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBaterB,
            this.menuCadastroCliente,
            this.menuCadastrarEstoque,
            this.menuCadastroFornecedor,
            this.menuCadastroFuncionario});
            this.cadastrarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem.Image")));
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.cadastrarToolStripMenuItem.Text = "Cadastro";
            // 
            // menuBaterB
            // 
            this.menuBaterB.Name = "menuBaterB";
            this.menuBaterB.Size = new System.Drawing.Size(138, 22);
            this.menuBaterB.Text = "Bater-Ponto";
            this.menuBaterB.Click += new System.EventHandler(this.menuBaterB_Click);
            // 
            // menuCadastroCliente
            // 
            this.menuCadastroCliente.Name = "menuCadastroCliente";
            this.menuCadastroCliente.Size = new System.Drawing.Size(138, 22);
            this.menuCadastroCliente.Text = "Cliente";
            this.menuCadastroCliente.Click += new System.EventHandler(this.menuCadastroC_Click);
            // 
            // menuCadastrarEstoque
            // 
            this.menuCadastrarEstoque.Name = "menuCadastrarEstoque";
            this.menuCadastrarEstoque.Size = new System.Drawing.Size(138, 22);
            this.menuCadastrarEstoque.Text = "Estoque";
            this.menuCadastrarEstoque.Click += new System.EventHandler(this.estoqueToolStripMenuItem_Click);
            // 
            // menuCadastroFornecedor
            // 
            this.menuCadastroFornecedor.Name = "menuCadastroFornecedor";
            this.menuCadastroFornecedor.Size = new System.Drawing.Size(138, 22);
            this.menuCadastroFornecedor.Text = "Fornecedor";
            this.menuCadastroFornecedor.Click += new System.EventHandler(this.menuCadastroF_Click);
            // 
            // menuCadastroFuncionario
            // 
            this.menuCadastroFuncionario.Name = "menuCadastroFuncionario";
            this.menuCadastroFuncionario.Size = new System.Drawing.Size(138, 22);
            this.menuCadastroFuncionario.Text = "Funcionário";
            this.menuCadastroFuncionario.Click += new System.EventHandler(this.menuCadastroFu_Click);
            // 
            // menuConsultaCc
            // 
            this.menuConsultaCc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuConsultaCliente,
            this.menuConsultarEstoque,
            this.menuConsultarFuncionario,
            this.menuConsultarFornecedor});
            this.menuConsultaCc.Image = ((System.Drawing.Image)(resources.GetObject("menuConsultaCc.Image")));
            this.menuConsultaCc.Name = "menuConsultaCc";
            this.menuConsultaCc.Size = new System.Drawing.Size(82, 20);
            this.menuConsultaCc.Text = "Consulta";
            // 
            // menuConsultaCliente
            // 
            this.menuConsultaCliente.Name = "menuConsultaCliente";
            this.menuConsultaCliente.Size = new System.Drawing.Size(137, 22);
            this.menuConsultaCliente.Text = "Cliente";
            this.menuConsultaCliente.Click += new System.EventHandler(this.menuConsultaC_Click);
            // 
            // menuConsultarEstoque
            // 
            this.menuConsultarEstoque.Name = "menuConsultarEstoque";
            this.menuConsultarEstoque.Size = new System.Drawing.Size(137, 22);
            this.menuConsultarEstoque.Text = "Estoque";
            this.menuConsultarEstoque.Click += new System.EventHandler(this.menuConsultarEstoque_Click);
            // 
            // menuConsultarFuncionario
            // 
            this.menuConsultarFuncionario.Name = "menuConsultarFuncionario";
            this.menuConsultarFuncionario.Size = new System.Drawing.Size(137, 22);
            this.menuConsultarFuncionario.Text = "Funcionário";
            this.menuConsultarFuncionario.Click += new System.EventHandler(this.menuConsultarFu_Click);
            // 
            // menuConsultarFornecedor
            // 
            this.menuConsultarFornecedor.Name = "menuConsultarFornecedor";
            this.menuConsultarFornecedor.Size = new System.Drawing.Size(137, 22);
            this.menuConsultarFornecedor.Text = "Fornecedor";
            this.menuConsultarFornecedor.Click += new System.EventHandler(this.menuConsultarF_Click);
            // 
            // pedidoVendaToolStripMenuItem
            // 
            this.pedidoVendaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidosToolStripMenuItem,
            this.produtosToolStripMenuItem});
            this.pedidoVendaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pedidoVendaToolStripMenuItem.Image")));
            this.pedidoVendaToolStripMenuItem.Name = "pedidoVendaToolStripMenuItem";
            this.pedidoVendaToolStripMenuItem.Size = new System.Drawing.Size(144, 20);
            this.pedidoVendaToolStripMenuItem.Text = "Módulo de Compras";
            // 
            // pedidosToolStripMenuItem
            // 
            this.pedidosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCadastrarPedidoCompra,
            this.menuConsultarPedidoCompra});
            this.pedidosToolStripMenuItem.Name = "pedidosToolStripMenuItem";
            this.pedidosToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.pedidosToolStripMenuItem.Text = "Pedidos";
            // 
            // menuCadastrarPedidoCompra
            // 
            this.menuCadastrarPedidoCompra.Name = "menuCadastrarPedidoCompra";
            this.menuCadastrarPedidoCompra.Size = new System.Drawing.Size(188, 22);
            this.menuCadastrarPedidoCompra.Text = "Cadastrar Pedido";
            this.menuCadastrarPedidoCompra.Click += new System.EventHandler(this.menuCadastrarPDDC_Click);
            // 
            // menuConsultarPedidoCompra
            // 
            this.menuConsultarPedidoCompra.Name = "menuConsultarPedidoCompra";
            this.menuConsultarPedidoCompra.Size = new System.Drawing.Size(188, 22);
            this.menuConsultarPedidoCompra.Text = "Consultar Cadastrado";
            this.menuConsultarPedidoCompra.Click += new System.EventHandler(this.menuConsultarPDDC_Click);
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCadastrarProdutoCompra,
            this.menuConsultarProdutoCompra});
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.produtosToolStripMenuItem.Text = "Produtos";
            // 
            // menuCadastrarProdutoCompra
            // 
            this.menuCadastrarProdutoCompra.Name = "menuCadastrarProdutoCompra";
            this.menuCadastrarProdutoCompra.Size = new System.Drawing.Size(188, 22);
            this.menuCadastrarProdutoCompra.Text = "Cadastrar Novo";
            this.menuCadastrarProdutoCompra.Click += new System.EventHandler(this.menuCadastrarPDTC_Click);
            // 
            // menuConsultarProdutoCompra
            // 
            this.menuConsultarProdutoCompra.Name = "menuConsultarProdutoCompra";
            this.menuConsultarProdutoCompra.Size = new System.Drawing.Size(188, 22);
            this.menuConsultarProdutoCompra.Text = "Consultar Cadastrado";
            this.menuConsultarProdutoCompra.Click += new System.EventHandler(this.menuConsultarPDTS_Click);
            // 
            // menuCadastrarPV
            // 
            this.menuCadastrarPV.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidosToolStripMenuItem1,
            this.consultarToolStripMenuItem1});
            this.menuCadastrarPV.Image = ((System.Drawing.Image)(resources.GetObject("menuCadastrarPV.Image")));
            this.menuCadastrarPV.Name = "menuCadastrarPV";
            this.menuCadastrarPV.Size = new System.Drawing.Size(133, 20);
            this.menuCadastrarPV.Text = "Módulo de Vendas";
            // 
            // pedidosToolStripMenuItem1
            // 
            this.pedidosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCadastrarPedidoVenda,
            this.menuConsultarPedidoVenda});
            this.pedidosToolStripMenuItem1.Name = "pedidosToolStripMenuItem1";
            this.pedidosToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.pedidosToolStripMenuItem1.Text = "Pedidos";
            // 
            // menuCadastrarPedidoVenda
            // 
            this.menuCadastrarPedidoVenda.Name = "menuCadastrarPedidoVenda";
            this.menuCadastrarPedidoVenda.Size = new System.Drawing.Size(188, 22);
            this.menuCadastrarPedidoVenda.Text = "Cadastrar Novo";
            this.menuCadastrarPedidoVenda.Click += new System.EventHandler(this.menuCadastrarPVS_Click);
            // 
            // menuConsultarPedidoVenda
            // 
            this.menuConsultarPedidoVenda.Name = "menuConsultarPedidoVenda";
            this.menuConsultarPedidoVenda.Size = new System.Drawing.Size(188, 22);
            this.menuConsultarPedidoVenda.Text = "Consultar Cadastrado";
            this.menuConsultarPedidoVenda.Click += new System.EventHandler(this.menuConsultarPV_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCadastrarProdutoVenda,
            this.menuConsultarProdutoVenda});
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.consultarToolStripMenuItem1.Text = "Produtos";
            // 
            // menuCadastrarProdutoVenda
            // 
            this.menuCadastrarProdutoVenda.Name = "menuCadastrarProdutoVenda";
            this.menuCadastrarProdutoVenda.Size = new System.Drawing.Size(188, 22);
            this.menuCadastrarProdutoVenda.Text = "Cadastrar Novo";
            this.menuCadastrarProdutoVenda.Click += new System.EventHandler(this.menuCadastrarPDT_Click);
            // 
            // menuConsultarProdutoVenda
            // 
            this.menuConsultarProdutoVenda.Name = "menuConsultarProdutoVenda";
            this.menuConsultarProdutoVenda.Size = new System.Drawing.Size(188, 22);
            this.menuConsultarProdutoVenda.Text = "Consultar Cadastrado";
            this.menuConsultarProdutoVenda.Click += new System.EventHandler(this.menuConsultarPDT_Click);
            // 
            // relatórioToolStripMenuItem
            // 
            this.relatórioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFolha,
            this.menuFluxo});
            this.relatórioToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("relatórioToolStripMenuItem.Image")));
            this.relatórioToolStripMenuItem.Name = "relatórioToolStripMenuItem";
            this.relatórioToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.relatórioToolStripMenuItem.Text = "Relatórios";
            // 
            // menuFolha
            // 
            this.menuFolha.Name = "menuFolha";
            this.menuFolha.Size = new System.Drawing.Size(183, 22);
            this.menuFolha.Text = "Folha de Pagamento";
            this.menuFolha.Click += new System.EventHandler(this.menuFolha_Click);
            // 
            // menuFluxo
            // 
            this.menuFluxo.Name = "menuFluxo";
            this.menuFluxo.Size = new System.Drawing.Size(183, 22);
            this.menuFluxo.Text = "Fluxo de Caixa";
            this.menuFluxo.Click += new System.EventHandler(this.menuFluxo_Click);
            // 
            // menuLogoff
            // 
            this.menuLogoff.Image = ((System.Drawing.Image)(resources.GetObject("menuLogoff.Image")));
            this.menuLogoff.Name = "menuLogoff";
            this.menuLogoff.Size = new System.Drawing.Size(70, 20);
            this.menuLogoff.Text = "Logoff";
            this.menuLogoff.Click += new System.EventHandler(this.menuLogoff_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.menuConsultaCc,
            this.pedidoVendaToolStripMenuItem,
            this.menuCadastrarPV,
            this.relatórioToolStripMenuItem,
            this.menuLogoff});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(606, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Maroon;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 489);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(603, 27);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // pnlConteudo
            // 
            this.pnlConteudo.BackColor = System.Drawing.Color.Snow;
            this.pnlConteudo.Location = new System.Drawing.Point(0, 53);
            this.pnlConteudo.Name = "pnlConteudo";
            this.pnlConteudo.Size = new System.Drawing.Size(604, 437);
            this.pnlConteudo.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGreen;
            this.panel1.Controls.Add(this.lblFechar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(603, 26);
            this.panel1.TabIndex = 18;
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold);
            this.lblFechar.ForeColor = System.Drawing.Color.White;
            this.lblFechar.Location = new System.Drawing.Point(581, 0);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(22, 23);
            this.lblFechar.TabIndex = 11;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Restaurante Puro Tempero";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.menuStrip2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(603, 27);
            this.panel2.TabIndex = 21;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(603, 516);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.pnlConteudo);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip2;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuBaterB;
        private System.Windows.Forms.ToolStripMenuItem menuCadastroCliente;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarEstoque;
        private System.Windows.Forms.ToolStripMenuItem menuCadastroFornecedor;
        private System.Windows.Forms.ToolStripMenuItem menuCadastroFuncionario;
        private System.Windows.Forms.ToolStripMenuItem menuConsultaCc;
        private System.Windows.Forms.ToolStripMenuItem menuConsultaCliente;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarEstoque;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarFuncionario;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarFornecedor;
        private System.Windows.Forms.ToolStripMenuItem pedidoVendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarPedidoCompra;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarPedidoCompra;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarProdutoCompra;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarProdutoCompra;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarPV;
        private System.Windows.Forms.ToolStripMenuItem pedidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarPedidoVenda;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarPedidoVenda;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuCadastrarProdutoVenda;
        private System.Windows.Forms.ToolStripMenuItem menuConsultarProdutoVenda;
        private System.Windows.Forms.ToolStripMenuItem relatórioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuFolha;
        private System.Windows.Forms.ToolStripMenuItem menuFluxo;
        private System.Windows.Forms.ToolStripMenuItem menuLogoff;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlConteudo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
    }
}