﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Cliente;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Funcionário;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Fornecedores;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Produto;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_Financeiro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms
{
    public partial class frmMenu : Form
    {
        public static frmMenu Atual;

        public frmMenu()
        {
            InitializeComponent();
            CarregarPermissoes();
            Atual = this;
        }

        private void CarregarPermissoes ()
        {
            //Caso ele não tenha permissão para Cadastrar
            if (UserSession.Logado.Salvar == false)
            {
                menuCadastroCliente.Enabled = false;
                menuCadastrarPedidoCompra.Enabled = false;
                menuCadastrarProdutoCompra.Enabled = false;
                menuCadastrarPedidoVenda.Enabled = false;
                menuCadastrarProdutoVenda.Enabled = false;  
                menuCadastroFornecedor.Enabled = false;
                menuCadastroFuncionario.Enabled = false;
                menuBaterB.Enabled = false;
                //menuReceberEstoque.Enabled = false;
            }

            //Caso ele não tenha permissão para Consultar
            if (UserSession.Logado.Consultar == false)
            {
                menuConsultaCliente.Enabled = false;
                menuConsultarPedidoCompra.Enabled = false;
                menuConsultarProdutoCompra.Enabled = false;
                menuConsultarPedidoVenda.Enabled = false;
                menuConsultarProdutoVenda.Enabled = false;
                menuConsultarFornecedor.Enabled = false;
                menuConsultarEstoque.Enabled = false;
                menuConsultarFuncionario.Enabled = false;
                menuFluxo.Enabled = false;
                menuFolha.Enabled = false;
            }

            //Outra coisa que eu pensei foi em relação a Deleter e alterar.
            //Basicamente, eu estou bloqueando a griedview nas telas que tem essa parte de alterar.
        }
        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (pnlConteudo.Controls.Count == 1)
                pnlConteudo.Controls.RemoveAt(0);
                pnlConteudo.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void menuCadastroC_Click(object sender, EventArgs e)
        {
            frmCadastrar_Cliente tela = new frmCadastrar_Cliente();
            OpenScreen(tela);
        }

        private void menuCadastroFu_Click(object sender, EventArgs e)
        {
            frmSalvar_Funcionario tela = new frmSalvar_Funcionario();
            OpenScreen(tela);
        }

        private void menuCadastroF_Click(object sender, EventArgs e)
        {
            frmCadastrar_Fornecedor tela = new frmCadastrar_Fornecedor();
            OpenScreen(tela);
        }

        private void menuBaterB_Click(object sender, EventArgs e)
        {
            frmBater_Ponto tela = new frmBater_Ponto();
            OpenScreen(tela);
        }

        private void menuConsultaC_Click(object sender, EventArgs e)
        {
            frmCadastrar_Cliente tela = new frmCadastrar_Cliente();
            OpenScreen(tela);
        }

        private void menuReceberE_Click(object sender, EventArgs e)
        {
            frmReceber_Compra tela = new frmReceber_Compra();
            OpenScreen(tela);
        }

        private void menuConsultarE_Click(object sender, EventArgs e)
        {
            frmConsultar_Estoque tela = new frmConsultar_Estoque();
            OpenScreen(tela);
        }

        private void menuConsultarFu_Click(object sender, EventArgs e)
        {
            frmConsultar_Funcionario tela = new frmConsultar_Funcionario();
            OpenScreen(tela);
        }

        private void menuConsultarF_Click(object sender, EventArgs e)
        {
            frmConsultar_Fornecedor tela = new frmConsultar_Fornecedor();
            OpenScreen(tela);
        }

        private void menuCadastrarPDDC_Click(object sender, EventArgs e)
        {
            frmCadastrar_Pedido tela = new frmCadastrar_Pedido();
            OpenScreen(tela);
        }

        private void menuConsultarPDDC_Click(object sender, EventArgs e)
        {
            frmConsultar_Pedido tela = new frmConsultar_Pedido();
            OpenScreen(tela);
        }

        private void menuCadastrarPDTC_Click(object sender, EventArgs e)
        {
            frmCadastrar_Produto tela = new frmCadastrar_Produto();
            OpenScreen(tela);
        }

        private void menuConsultarPDTS_Click(object sender, EventArgs e)
        {
            frmConsultar_Produtos tela = new frmConsultar_Produtos();
            OpenScreen(tela);
        }

        private void menuCadastrarPVS_Click(object sender, EventArgs e)
        {
            frmCadastrar_PedidoVenda tela = new frmCadastrar_PedidoVenda();
            OpenScreen(tela);
        }

        private void menuConsultarPV_Click(object sender, EventArgs e)
        {
            frmConsultar_PedidoVenda tela = new frmConsultar_PedidoVenda();
            OpenScreen(tela);
        }

        private void menuCadastrarPDT_Click(object sender, EventArgs e)
        {
            frmCadastrar_ProdutoVenda tela = new frmCadastrar_ProdutoVenda();
            OpenScreen(tela);
        }

        private void menuConsultarPDT_Click(object sender, EventArgs e)
        {
            frmConsultar_ProdutoVenda tela = new frmConsultar_ProdutoVenda();
            OpenScreen(tela);
        }

        private void menuFolha_Click(object sender, EventArgs e)
        {
            frmFolha_Pagamento tela = new frmFolha_Pagamento();
            OpenScreen(tela);
        }

        private void menuFluxo_Click(object sender, EventArgs e)
        {
            frmFluxoCaixa tela = new frmFluxoCaixa();
            OpenScreen(tela);
        }

        private void menuLogoff_Click(object sender, EventArgs e)
        {
            frmLog tela = new frmLog();
            tela.Show();
            this.Hide();
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReceber_Compra tela = new frmReceber_Compra();
            OpenScreen(tela);
        }

        private void menuConsultarEstoque_Click(object sender, EventArgs e)
        {
            frmConsultar_Estoque tela = new frmConsultar_Estoque();
            OpenScreen(tela);
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
