﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLog());
        }
    }
}
