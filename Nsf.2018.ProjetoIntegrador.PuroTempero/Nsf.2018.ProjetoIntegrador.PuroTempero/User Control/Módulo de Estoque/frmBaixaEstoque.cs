﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    public partial class frmBaixaEstoque : UserControl
    {
        public frmBaixaEstoque()
        {
            InitializeComponent();
            CarregarCombo();
            //Estoque alterado com sucesso!!
        }
        private void CarregarCombo()
        {
            Business_ProdutoCompra db = new Business_ProdutoCompra();
            List<DTO_ProdutoCompra> produto = db.Listar();

            cboProduto.ValueMember = nameof(DTO_ProdutoCompra.ID);
            cboProduto.DisplayMember = nameof(DTO_ProdutoCompra.Produto);

            cboProduto.DataSource = produto;
        }
    }
}
