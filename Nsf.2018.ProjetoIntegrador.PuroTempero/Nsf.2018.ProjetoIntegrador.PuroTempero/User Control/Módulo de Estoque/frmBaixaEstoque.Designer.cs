﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    partial class frmBaixaEstoque
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbBaixa = new System.Windows.Forms.GroupBox();
            this.cboProduto = new System.Windows.Forms.ComboBox();
            this.lblProduto = new System.Windows.Forms.Label();
            this.lblPAtual = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.gpbInformações = new System.Windows.Forms.GroupBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.rtxtInformacao = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gpbBaixa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.gpbInformações.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbBaixa
            // 
            this.gpbBaixa.Controls.Add(this.label3);
            this.gpbBaixa.Controls.Add(this.gpbInformações);
            this.gpbBaixa.Controls.Add(this.numericUpDown1);
            this.gpbBaixa.Controls.Add(this.button1);
            this.gpbBaixa.Controls.Add(this.pictureBox1);
            this.gpbBaixa.Controls.Add(this.label1);
            this.gpbBaixa.Controls.Add(this.label2);
            this.gpbBaixa.Controls.Add(this.lblPAtual);
            this.gpbBaixa.Controls.Add(this.lblProduto);
            this.gpbBaixa.Controls.Add(this.cboProduto);
            this.gpbBaixa.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbBaixa.Location = new System.Drawing.Point(3, 3);
            this.gpbBaixa.Name = "gpbBaixa";
            this.gpbBaixa.Size = new System.Drawing.Size(601, 429);
            this.gpbBaixa.TabIndex = 0;
            this.gpbBaixa.TabStop = false;
            this.gpbBaixa.Text = "Baixa no Estoque ";
            // 
            // cboProduto
            // 
            this.cboProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProduto.FormattingEnabled = true;
            this.cboProduto.Location = new System.Drawing.Point(30, 75);
            this.cboProduto.Name = "cboProduto";
            this.cboProduto.Size = new System.Drawing.Size(251, 28);
            this.cboProduto.TabIndex = 0;
            // 
            // lblProduto
            // 
            this.lblProduto.AutoSize = true;
            this.lblProduto.Location = new System.Drawing.Point(26, 52);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(63, 20);
            this.lblProduto.TabIndex = 1;
            this.lblProduto.Text = "Produto:";
            // 
            // lblPAtual
            // 
            this.lblPAtual.AutoSize = true;
            this.lblPAtual.Location = new System.Drawing.Point(26, 146);
            this.lblPAtual.Name = "lblPAtual";
            this.lblPAtual.Size = new System.Drawing.Size(45, 20);
            this.lblPAtual.TabIndex = 1;
            this.lblPAtual.Text = "Atual:";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(298, 187);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 26);
            this.button1.TabIndex = 3;
            this.button1.Text = "Buscar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Taco;
            this.pictureBox1.Location = new System.Drawing.Point(298, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 146);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "--";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(30, 241);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(251, 26);
            this.numericUpDown1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Quantidade:";
            // 
            // gpbInformações
            // 
            this.gpbInformações.Controls.Add(this.lblOBS);
            this.gpbInformações.Controls.Add(this.imgImagem);
            this.gpbInformações.Controls.Add(this.rtxtInformacao);
            this.gpbInformações.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformações.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformações.Location = new System.Drawing.Point(413, 25);
            this.gpbInformações.Name = "gpbInformações";
            this.gpbInformações.Size = new System.Drawing.Size(182, 389);
            this.gpbInformações.TabIndex = 33;
            this.gpbInformações.TabStop = false;
            this.gpbInformações.Text = "Informações ";
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(6, 250);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 30;
            this.lblOBS.Text = "OBS:";
            // 
            // imgImagem
            // 
            this.imgImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Registro;
            this.imgImagem.Location = new System.Drawing.Point(19, 35);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(141, 205);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // rtxtInformacao
            // 
            this.rtxtInformacao.Enabled = false;
            this.rtxtInformacao.Font = new System.Drawing.Font("Arial", 10F);
            this.rtxtInformacao.Location = new System.Drawing.Point(9, 270);
            this.rtxtInformacao.Name = "rtxtInformacao";
            this.rtxtInformacao.Size = new System.Drawing.Size(167, 107);
            this.rtxtInformacao.TabIndex = 27;
            this.rtxtInformacao.Text = "Essa tela servirá como meio de fazer uma subtração da quantidade de itens alocado" +
    "s dentro do estoque.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 325);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 20);
            this.label3.TabIndex = 34;
            this.label3.Text = " ";
            // 
            // frmBaixaEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbBaixa);
            this.Name = "frmBaixaEstoque";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbBaixa.ResumeLayout(false);
            this.gpbBaixa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.gpbInformações.ResumeLayout(false);
            this.gpbInformações.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbBaixa;
        private System.Windows.Forms.Label lblPAtual;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.ComboBox cboProduto;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gpbInformações;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.RichTextBox rtxtInformacao;
    }
}
