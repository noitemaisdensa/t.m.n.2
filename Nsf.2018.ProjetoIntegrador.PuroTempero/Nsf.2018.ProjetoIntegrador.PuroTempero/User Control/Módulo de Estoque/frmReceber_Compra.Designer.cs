﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    partial class frmReceber_Compra
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbReceberCompra = new System.Windows.Forms.GroupBox();
            this.imgPesquisa = new System.Windows.Forms.PictureBox();
            this.NudPedido = new System.Windows.Forms.NumericUpDown();
            this.dgvEstoque = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReceberEstoque = new System.Windows.Forms.Button();
            this.lblNumeroPedido = new System.Windows.Forms.Label();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbInformações = new System.Windows.Forms.GroupBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.rtxtInformacao = new System.Windows.Forms.RichTextBox();
            this.gpbReceberCompra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPesquisa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstoque)).BeginInit();
            this.gpbInformações.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbReceberCompra
            // 
            this.gpbReceberCompra.Controls.Add(this.imgPesquisa);
            this.gpbReceberCompra.Controls.Add(this.NudPedido);
            this.gpbReceberCompra.Controls.Add(this.dgvEstoque);
            this.gpbReceberCompra.Controls.Add(this.btnReceberEstoque);
            this.gpbReceberCompra.Controls.Add(this.lblNumeroPedido);
            this.gpbReceberCompra.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbReceberCompra.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbReceberCompra.Location = new System.Drawing.Point(11, 6);
            this.gpbReceberCompra.Name = "gpbReceberCompra";
            this.gpbReceberCompra.Size = new System.Drawing.Size(397, 389);
            this.gpbReceberCompra.TabIndex = 29;
            this.gpbReceberCompra.TabStop = false;
            this.gpbReceberCompra.Text = "Estoque - Receber Compra ";
            this.gpbReceberCompra.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBox1_Paint);
            // 
            // imgPesquisa
            // 
            this.imgPesquisa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgPesquisa.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Alternative_Search;
            this.imgPesquisa.Location = new System.Drawing.Point(363, 35);
            this.imgPesquisa.Name = "imgPesquisa";
            this.imgPesquisa.Size = new System.Drawing.Size(25, 26);
            this.imgPesquisa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPesquisa.TabIndex = 59;
            this.imgPesquisa.TabStop = false;
            this.imgPesquisa.Click += new System.EventHandler(this.imgPesquisa_Click);
            // 
            // NudPedido
            // 
            this.NudPedido.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NudPedido.Location = new System.Drawing.Point(84, 35);
            this.NudPedido.Name = "NudPedido";
            this.NudPedido.Size = new System.Drawing.Size(275, 26);
            this.NudPedido.TabIndex = 25;
            this.NudPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dgvEstoque
            // 
            this.dgvEstoque.AllowUserToAddRows = false;
            this.dgvEstoque.AllowUserToDeleteRows = false;
            this.dgvEstoque.AllowUserToResizeColumns = false;
            this.dgvEstoque.AllowUserToResizeRows = false;
            this.dgvEstoque.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvEstoque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEstoque.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgvEstoque.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvEstoque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstoque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1,
            this.produto,
            this.marca,
            this.quantidade,
            this.precounit,
            this.Column3});
            this.dgvEstoque.GridColor = System.Drawing.Color.Snow;
            this.dgvEstoque.Location = new System.Drawing.Point(13, 123);
            this.dgvEstoque.MultiSelect = false;
            this.dgvEstoque.Name = "dgvEstoque";
            this.dgvEstoque.ReadOnly = true;
            this.dgvEstoque.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvEstoque.RowHeadersVisible = false;
            this.dgvEstoque.Size = new System.Drawing.Size(375, 254);
            this.dgvEstoque.TabIndex = 24;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ID";
            this.Column2.HeaderText = "ID";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 40;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Fornecedor";
            this.Column1.HeaderText = "Fornecedor";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // produto
            // 
            this.produto.DataPropertyName = "Produto";
            this.produto.HeaderText = "Produto";
            this.produto.Name = "produto";
            this.produto.ReadOnly = true;
            this.produto.Width = 200;
            // 
            // marca
            // 
            this.marca.DataPropertyName = "Marca";
            this.marca.HeaderText = "Marca";
            this.marca.Name = "marca";
            this.marca.ReadOnly = true;
            this.marca.Width = 150;
            // 
            // quantidade
            // 
            this.quantidade.DataPropertyName = "Quantidade";
            this.quantidade.HeaderText = "Quantidade";
            this.quantidade.Name = "quantidade";
            this.quantidade.ReadOnly = true;
            // 
            // precounit
            // 
            this.precounit.DataPropertyName = "Preco";
            this.precounit.HeaderText = "Preço Uni.";
            this.precounit.Name = "precounit";
            this.precounit.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Total";
            this.Column3.HeaderText = "Total";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // btnReceberEstoque
            // 
            this.btnReceberEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReceberEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceberEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceberEstoque.ForeColor = System.Drawing.Color.Black;
            this.btnReceberEstoque.Location = new System.Drawing.Point(243, 79);
            this.btnReceberEstoque.Name = "btnReceberEstoque";
            this.btnReceberEstoque.Size = new System.Drawing.Size(145, 27);
            this.btnReceberEstoque.TabIndex = 3;
            this.btnReceberEstoque.Text = "Receber no Estoque";
            this.btnReceberEstoque.UseVisualStyleBackColor = true;
            this.btnReceberEstoque.Click += new System.EventHandler(this.btnReceberEstoque_Click);
            // 
            // lblNumeroPedido
            // 
            this.lblNumeroPedido.AutoSize = true;
            this.lblNumeroPedido.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPedido.ForeColor = System.Drawing.Color.Black;
            this.lblNumeroPedido.Location = new System.Drawing.Point(9, 37);
            this.lblNumeroPedido.Name = "lblNumeroPedido";
            this.lblNumeroPedido.Size = new System.Drawing.Size(75, 20);
            this.lblNumeroPedido.TabIndex = 17;
            this.lblNumeroPedido.Text = "Nº Pedido:";
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(517, 401);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(79, 27);
            this.btnVoltar.TabIndex = 30;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // gpbInformações
            // 
            this.gpbInformações.Controls.Add(this.lblOBS);
            this.gpbInformações.Controls.Add(this.imgImagem);
            this.gpbInformações.Controls.Add(this.rtxtInformacao);
            this.gpbInformações.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformações.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformações.Location = new System.Drawing.Point(414, 6);
            this.gpbInformações.Name = "gpbInformações";
            this.gpbInformações.Size = new System.Drawing.Size(182, 389);
            this.gpbInformações.TabIndex = 32;
            this.gpbInformações.TabStop = false;
            this.gpbInformações.Text = "Informações ";
            this.gpbInformações.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBox1_Paint);
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(6, 250);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 30;
            this.lblOBS.Text = "OBS:";
            // 
            // imgImagem
            // 
            this.imgImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Registro;
            this.imgImagem.Location = new System.Drawing.Point(19, 35);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(141, 205);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // rtxtInformacao
            // 
            this.rtxtInformacao.Enabled = false;
            this.rtxtInformacao.Font = new System.Drawing.Font("Arial", 10F);
            this.rtxtInformacao.Location = new System.Drawing.Point(9, 270);
            this.rtxtInformacao.Name = "rtxtInformacao";
            this.rtxtInformacao.Size = new System.Drawing.Size(167, 107);
            this.rtxtInformacao.TabIndex = 27;
            this.rtxtInformacao.Text = "Essa tela serve como modelo para fazer a verificação real da quantidade de produt" +
    "os que estão armazenados no estoque";
            // 
            // frmReceber_Compra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbInformações);
            this.Controls.Add(this.gpbReceberCompra);
            this.Controls.Add(this.btnVoltar);
            this.Name = "frmReceber_Compra";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbReceberCompra.ResumeLayout(false);
            this.gpbReceberCompra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPesquisa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstoque)).EndInit();
            this.gpbInformações.ResumeLayout(false);
            this.gpbInformações.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbReceberCompra;
        private System.Windows.Forms.NumericUpDown NudPedido;
        private System.Windows.Forms.DataGridView dgvEstoque;
        private System.Windows.Forms.Button btnReceberEstoque;
        private System.Windows.Forms.Label lblNumeroPedido;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox gpbInformações;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.RichTextBox rtxtInformacao;
        private System.Windows.Forms.PictureBox imgPesquisa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn precounit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}
