﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Fornecedores
{
    partial class frmCadastrar_Fornecedor
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVoltar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.gbpFabricante = new System.Windows.Forms.GroupBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.txtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.gpbInformações = new System.Windows.Forms.GroupBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.rtxtInformacao = new System.Windows.Forms.RichTextBox();
            this.gpbEndereco = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.imagePesquisa = new System.Windows.Forms.PictureBox();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.gbpFabricante.SuspendLayout();
            this.gpbInformações.SuspendLayout();
            this.gpbEndereco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(438, 402);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(75, 28);
            this.btnVoltar.TabIndex = 3;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.DarkGreen;
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(519, 402);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 28);
            this.btnSalvar.TabIndex = 2;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // gbpFabricante
            // 
            this.gbpFabricante.Controls.Add(this.txtNome);
            this.gbpFabricante.Controls.Add(this.txtEmail);
            this.gbpFabricante.Controls.Add(this.txtTelefone);
            this.gbpFabricante.Controls.Add(this.lblEmail);
            this.gbpFabricante.Controls.Add(this.lblTelefone);
            this.gbpFabricante.Controls.Add(this.txtCNPJ);
            this.gbpFabricante.Controls.Add(this.lblNome);
            this.gbpFabricante.Controls.Add(this.lblCPF);
            this.gbpFabricante.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gbpFabricante.ForeColor = System.Drawing.Color.DarkGreen;
            this.gbpFabricante.Location = new System.Drawing.Point(13, -4);
            this.gbpFabricante.Name = "gbpFabricante";
            this.gbpFabricante.Size = new System.Drawing.Size(413, 172);
            this.gbpFabricante.TabIndex = 0;
            this.gbpFabricante.TabStop = false;
            this.gbpFabricante.Text = "Fornecedor";
            this.gbpFabricante.Paint += new System.Windows.Forms.PaintEventHandler(this.gbpFabricante_Paint);
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(28, 43);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(331, 22);
            this.txtNome.TabIndex = 0;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(178, 135);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(181, 22);
            this.txtEmail.TabIndex = 3;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.Location = new System.Drawing.Point(28, 135);
            this.txtTelefone.Mask = "(99) 99999-9999";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(128, 22);
            this.txtTelefone.TabIndex = 2;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.Black;
            this.lblEmail.Location = new System.Drawing.Point(174, 115);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(49, 20);
            this.lblEmail.TabIndex = 5;
            this.lblEmail.Text = "Email:";
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.ForeColor = System.Drawing.Color.Black;
            this.lblTelefone.Location = new System.Drawing.Point(24, 115);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(66, 20);
            this.lblTelefone.TabIndex = 5;
            this.lblTelefone.Text = "Telefone:";
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNPJ.ForeColor = System.Drawing.Color.Black;
            this.txtCNPJ.Location = new System.Drawing.Point(28, 89);
            this.txtCNPJ.Mask = "00,000,000/0000-00";
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(128, 22);
            this.txtCNPJ.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.Black;
            this.lblNome.Location = new System.Drawing.Point(24, 23);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(68, 20);
            this.lblNome.TabIndex = 4;
            this.lblNome.Text = "Empresa:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.Black;
            this.lblCPF.Location = new System.Drawing.Point(26, 69);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(47, 20);
            this.lblCPF.TabIndex = 5;
            this.lblCPF.Text = "CNPJ:";
            // 
            // gpbInformações
            // 
            this.gpbInformações.Controls.Add(this.lblOBS);
            this.gpbInformações.Controls.Add(this.imgImagem);
            this.gpbInformações.Controls.Add(this.rtxtInformacao);
            this.gpbInformações.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformações.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformações.Location = new System.Drawing.Point(436, -4);
            this.gpbInformações.Name = "gpbInformações";
            this.gpbInformações.Size = new System.Drawing.Size(158, 402);
            this.gpbInformações.TabIndex = 5;
            this.gpbInformações.TabStop = false;
            this.gpbInformações.Text = "Informações ";
            this.gpbInformações.Paint += new System.Windows.Forms.PaintEventHandler(this.gbpFabricante_Paint);
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(6, 229);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 0;
            this.lblOBS.Text = "OBS:";
            // 
            // rtxtInformacao
            // 
            this.rtxtInformacao.Enabled = false;
            this.rtxtInformacao.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtInformacao.Location = new System.Drawing.Point(11, 249);
            this.rtxtInformacao.Name = "rtxtInformacao";
            this.rtxtInformacao.Size = new System.Drawing.Size(139, 136);
            this.rtxtInformacao.TabIndex = 1;
            this.rtxtInformacao.Text = "";
            // 
            // gpbEndereco
            // 
            this.gpbEndereco.Controls.Add(this.txtCidade);
            this.gpbEndereco.Controls.Add(this.cboUF);
            this.gpbEndereco.Controls.Add(this.lblCidade);
            this.gpbEndereco.Controls.Add(this.lblUF);
            this.gpbEndereco.Controls.Add(this.imagePesquisa);
            this.gpbEndereco.Controls.Add(this.nudNumero);
            this.gpbEndereco.Controls.Add(this.txtCEP);
            this.gpbEndereco.Controls.Add(this.lblNumero);
            this.gpbEndereco.Controls.Add(this.txtComplemento);
            this.gpbEndereco.Controls.Add(this.lblCompemento);
            this.gpbEndereco.Controls.Add(this.lblEndereco);
            this.gpbEndereco.Controls.Add(this.txtEndereco);
            this.gpbEndereco.Controls.Add(this.lblCEP);
            this.gpbEndereco.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEndereco.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbEndereco.Location = new System.Drawing.Point(13, 168);
            this.gpbEndereco.Name = "gpbEndereco";
            this.gpbEndereco.Size = new System.Drawing.Size(413, 230);
            this.gpbEndereco.TabIndex = 1;
            this.gpbEndereco.TabStop = false;
            this.gpbEndereco.Text = "Endereço ";
            this.gpbEndereco.Paint += new System.Windows.Forms.PaintEventHandler(this.gbpFabricante_Paint);
            // 
            // txtCidade
            // 
            this.txtCidade.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.ForeColor = System.Drawing.Color.Black;
            this.txtCidade.Location = new System.Drawing.Point(206, 189);
            this.txtCidade.MaxLength = 25;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(153, 22);
            this.txtCidade.TabIndex = 7;
            // 
            // cboUF
            // 
            this.cboUF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboUF.ForeColor = System.Drawing.Color.Black;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(28, 189);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(153, 24);
            this.cboUF.TabIndex = 6;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.ForeColor = System.Drawing.Color.Black;
            this.lblCidade.Location = new System.Drawing.Point(202, 170);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(56, 20);
            this.lblCidade.TabIndex = 1;
            this.lblCidade.Text = "Cidade:";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.ForeColor = System.Drawing.Color.Black;
            this.lblUF.Location = new System.Drawing.Point(24, 170);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(30, 20);
            this.lblUF.TabIndex = 0;
            this.lblUF.Text = "UF:";
            // 
            // nudNumero
            // 
            this.nudNumero.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudNumero.ForeColor = System.Drawing.Color.Black;
            this.nudNumero.Location = new System.Drawing.Point(206, 141);
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(153, 22);
            this.nudNumero.TabIndex = 5;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.ForeColor = System.Drawing.Color.Black;
            this.txtCEP.Location = new System.Drawing.Point(28, 45);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(96, 22);
            this.txtCEP.TabIndex = 1;
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.ForeColor = System.Drawing.Color.Black;
            this.lblNumero.Location = new System.Drawing.Point(202, 121);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(32, 20);
            this.lblNumero.TabIndex = 11;
            this.lblNumero.Text = "N° :";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(28, 141);
            this.txtComplemento.MaxLength = 10000;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(153, 22);
            this.txtComplemento.TabIndex = 4;
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompemento.ForeColor = System.Drawing.Color.Black;
            this.lblCompemento.Location = new System.Drawing.Point(24, 121);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(100, 20);
            this.lblCompemento.TabIndex = 10;
            this.lblCompemento.Text = "Complemento:";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.ForeColor = System.Drawing.Color.Black;
            this.lblEndereco.Location = new System.Drawing.Point(24, 73);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(72, 20);
            this.lblEndereco.TabIndex = 9;
            this.lblEndereco.Text = "Endereço:";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(28, 93);
            this.txtEndereco.MaxLength = 70;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(331, 22);
            this.txtEndereco.TabIndex = 3;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.ForeColor = System.Drawing.Color.Black;
            this.lblCEP.Location = new System.Drawing.Point(24, 26);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(40, 20);
            this.lblCEP.TabIndex = 8;
            this.lblCEP.Text = "CEP:";
            // 
            // lblMensagem
            // 
            this.lblMensagem.AutoSize = true;
            this.lblMensagem.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensagem.Location = new System.Drawing.Point(133, 406);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(0, 20);
            this.lblMensagem.TabIndex = 4;
            // 
            // imagePesquisa
            // 
            this.imagePesquisa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imagePesquisa.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Alternative_Search;
            this.imagePesquisa.Location = new System.Drawing.Point(133, 45);
            this.imagePesquisa.Name = "imagePesquisa";
            this.imagePesquisa.Size = new System.Drawing.Size(23, 23);
            this.imagePesquisa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagePesquisa.TabIndex = 42;
            this.imagePesquisa.TabStop = false;
            this.imagePesquisa.Click += new System.EventHandler(this.imagePesquisa_Click);
            // 
            // imgImagem
            // 
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Pesquisa;
            this.imgImagem.Location = new System.Drawing.Point(11, 23);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(139, 190);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // frmCadastrar_Fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblMensagem);
            this.Controls.Add(this.gpbEndereco);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.gbpFabricante);
            this.Controls.Add(this.gpbInformações);
            this.Name = "frmCadastrar_Fornecedor";
            this.Size = new System.Drawing.Size(607, 435);
            this.Load += new System.EventHandler(this.frmCadastrar_Fornecedor_Load);
            this.gbpFabricante.ResumeLayout(false);
            this.gbpFabricante.PerformLayout();
            this.gpbInformações.ResumeLayout(false);
            this.gpbInformações.PerformLayout();
            this.gpbEndereco.ResumeLayout(false);
            this.gpbEndereco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnVoltar;
        public System.Windows.Forms.Button btnSalvar;
        public System.Windows.Forms.GroupBox gbpFabricante;
        public System.Windows.Forms.GroupBox gpbInformações;
        public System.Windows.Forms.Label lblOBS;
        public System.Windows.Forms.PictureBox imgImagem;
        public System.Windows.Forms.RichTextBox rtxtInformacao;
        public System.Windows.Forms.TextBox txtNome;
        public System.Windows.Forms.MaskedTextBox txtTelefone;
        public System.Windows.Forms.Label lblTelefone;
        public System.Windows.Forms.MaskedTextBox txtCNPJ;
        public System.Windows.Forms.Label lblNome;
        public System.Windows.Forms.Label lblCPF;
        public System.Windows.Forms.GroupBox gpbEndereco;
        public System.Windows.Forms.ComboBox cboUF;
        public System.Windows.Forms.Label lblCidade;
        public System.Windows.Forms.Label lblUF;
        public System.Windows.Forms.PictureBox imagePesquisa;
        public System.Windows.Forms.NumericUpDown nudNumero;
        public System.Windows.Forms.MaskedTextBox txtCEP;
        public System.Windows.Forms.Label lblNumero;
        public System.Windows.Forms.TextBox txtComplemento;
        public System.Windows.Forms.Label lblCompemento;
        public System.Windows.Forms.Label lblEndereco;
        public System.Windows.Forms.TextBox txtEndereco;
        public System.Windows.Forms.Label lblCEP;
        public System.Windows.Forms.Label lblMensagem;
        public System.Windows.Forms.TextBox txtCidade;
        public System.Windows.Forms.MaskedTextBox txtEmail;
        public System.Windows.Forms.Label lblEmail;
    }
}
