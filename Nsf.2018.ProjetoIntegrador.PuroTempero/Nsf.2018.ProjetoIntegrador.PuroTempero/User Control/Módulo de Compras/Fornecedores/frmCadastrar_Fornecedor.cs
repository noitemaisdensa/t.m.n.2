﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Localização;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Fornecedores
{
    public partial class frmCadastrar_Fornecedor : UserControl
    {
        public frmCadastrar_Fornecedor()
        {
            InitializeComponent();

            Localizacao uf = new Localizacao();
            cboUF.DataSource = uf.UF();
        }

        private void gbpFabricante_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
        public void Salvar (DTO_Fornecedor dto)
        {
            //Pego o DTO como Parâmetro, após isso, passo o valor encontrado nos controles para o DTO
            dto.Razao_Social = txtNome.Text;
            dto.CNPJ = txtCNPJ.Text;
            dto.Telefone = txtTelefone.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Cidade = txtCidade.Text;
            dto.Email = txtEmail.Text;
            dto.Numero_Casa = nudNumero.Value;
            dto.UF = cboUF.SelectedItem.ToString();

            //Chamo a busines e passo o valor do DTO para o método Salvar
            Business_Fornecedor db = new Business_Fornecedor();
            db.Salvar(dto);

            //Troco as cores da label e mostro uma mensagem.
            lblMensagem.BackColor = Color.ForestGreen;
            lblMensagem.ForeColor = Color.White;
            lblMensagem.Text = "Fornecedor salvo com sucesso!!";
            lblMensagem.Controls.Clear();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //Instacio o DTO e chamo o método Salvar.
            DTO_Fornecedor dto = new DTO_Fornecedor();
            Salvar(dto);
        }

        private void imagePesquisa_Click(object sender, EventArgs e)
        {
            //Aqui irá ficar a API do Correios.
            //OBS: Não coloquei agora, pois a NET caiu.
        }

        private void frmCadastrar_Fornecedor_Load(object sender, EventArgs e)
        {

        }
    }
}
