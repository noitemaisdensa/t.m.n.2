﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Produto
{
    public partial class frmAlterar_Produto : UserControl
    {

        Business_ProdutoCompra db = new Business_ProdutoCompra();

        public frmAlterar_Produto()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void CarregarCombos ()
        {
            Business_Fornecedor db = new Business_Fornecedor();
            List<DTO_Fornecedor> list = db.Listar();

            cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);

            cboFornecedor.DataSource = list;
        }

        DTO_ProdutoCompra dto;

        //Pegando o valor de registro individual, selecionado pela gried no formulário de Consulta.
        public void LoadScreen(DTO_ProdutoCompra dto)
        {
            //Fazendo o DTO de escopo receber o valor de parâmetro
            this.dto = dto;

            //Passando o valor para os controles
            cboFornecedor.SelectedItem = dto.ID_Fornecedor;
            lblID.Text = dto.ID.ToString();
            txtProduto.Text = dto.Produto;
            txtMarca.Text = dto.Marca;
            nudPreco.Value = dto.Preco;
            imgGIF.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmConsultar_Produtos tela = new frmConsultar_Produtos();
            frmMenu.Atual.OpenScreen(tela);
            this.Hide();
        }

        private void gbpProduto_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void Alterar(DTO_ProdutoCompra dto)
        {
            //Pegando o DTO do Fornecedor
            DTO_Fornecedor fornecedor = cboFornecedor.SelectedItem as DTO_Fornecedor;

            //Passo o valor dos controles para o DTO.
            dto.ID = int.Parse(lblID.Text);
            dto.ID_Fornecedor = fornecedor.ID;
            dto.Produto = txtProduto.Text;
            dto.Preco = nudPreco.Value;
            dto.Marca = txtMarca.Text;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgGIF.Image);

            //Passo o valor do DTO para business.
            db.Alterar(dto);

            //Trocando as propriedades da label
            lblMensagem.ForeColor = Color.White;
            lblMensagem.BackColor = Color.ForestGreen;

            //Mostrando o valor para o usuário.
            lblMensagem.Text = "Produto Alterado com Sucesso!";
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //Passo o valor do DTO para o método Salvar
            DTO_ProdutoCompra dto = new DTO_ProdutoCompra();
            Alterar(dto);
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            //Instâncio a função que irá abrir os documentos Windows/Desktop.
            OpenFileDialog dialog = new OpenFileDialog();

            //Faz um filtro apenas em JPAG ou PNG
            dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";

            //Habilita a caixa de arquivos Windows.
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                //Pega a localização da imagem
                imgGIF.ImageLocation = dialog.FileName;
            }
        }
    }
}
