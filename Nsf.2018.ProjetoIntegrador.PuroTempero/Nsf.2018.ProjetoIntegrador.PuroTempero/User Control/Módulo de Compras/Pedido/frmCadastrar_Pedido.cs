﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra
{
    public partial class frmCadastrar_Pedido : UserControl
    {
        //É essa maravilha que irá fazer todo o nosso trabalho... Pois a mesma irá irá trazer todos os valores_
        //Encontrados na combobox selecionada e transforma em uma lista na griedview 
        BindingList<DTO_ProdutoCompra> produtosCarrinho = new BindingList<DTO_ProdutoCompra>();

        public frmCadastrar_Pedido()
        {
            InitializeComponent();

            //Carregando os métodos
            CarregarCombo();
            CarregarGrid();
        }

        //Método para carregar o valor da gried sempre que for alterado
        private void CarregarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho; 
        }

        //Método para carregar o valor encontrado no método de listar
        private void CarregarCombo()
        {
            Business_ProdutoCompra db = new Business_ProdutoCompra();
            List<DTO_ProdutoCompra> produtos = db.Listar();

            cboProduto.ValueMember = nameof(DTO_ProdutoCompra.ID);
            cboProduto.DisplayMember = nameof(DTO_ProdutoCompra.Produto);

            cboProduto.DataSource = produtos;

            // ======================================================= //

            Business_Fornecedor banco = new Business_Fornecedor();
            List<DTO_Fornecedor> empresas = banco.Listar();

            cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);

            cboFornecedor.DataSource = empresas;
        }


        private void gbpProduto_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnConcluir_Click(object sender, EventArgs e)
        {
            //Pegando o valor da combo
            DTO_Fornecedor forncedor = cboFornecedor.SelectedItem as DTO_Fornecedor;
            DTO_Compra dto = new DTO_Compra();

            //Pegando os valores dos controles
            dto.Data_Compra = dtpPedido.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            dto.ID_Fornecedor = forncedor.ID;

            Business_Compra db = new Business_Compra();
            db.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso!!", 
                            "Puro Tempero",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void btnVoltar_Click_1(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void lblAdicionar_Click_1(object sender, EventArgs e)
        {
            DTO_ProdutoCompra dto = cboProduto.SelectedItem as DTO_ProdutoCompra;
            int qtd = Convert.ToInt32(nudQuantidade.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        //Evento que move os valores
        private void cboProduto_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Pego o valor selecionado no combo
            DTO_ProdutoCompra produto = cboProduto.SelectedItem as DTO_ProdutoCompra;

            //Carrego a label de preço com o valor equivalente na combo
            lblPreco.Text = "R$: " + produto.Preco.ToString();

            //Faço uma conta do valor total
            decimal total = produto.Preco * nudQuantidade.Value;

            //Carrego a label de total com o valor equivalente na combo, não esquecendo de pegar a variável com o valor acima
            lblTotal.Text = "R$: " + total.ToString();

            //Carrego a picture box com o valor equivalente na combo
            imgProduto.Image = ImagemPlugin.ConverterParaImagem(produto.Imagem);
        }

        //Semelhante ao evento acima da combo... O mesmo traz o valor equivalente ao número que o mesmo está pedindo
        private void nudQuantidade_ValueChanged_1(object sender, EventArgs e)
        {
            //Pego o valor selecionado no combo
            DTO_ProdutoCompra produto = cboProduto.SelectedItem as DTO_ProdutoCompra;

            //Faço uma conta do valor total
            decimal total = produto.Preco * nudQuantidade.Value;

            //Carrego a label de total com o valor equivalente na combo, não esquecendo de pegar a variável com o valor acima
            lblTotal.Text = "R$: " + total.ToString();
        }
    }
}
