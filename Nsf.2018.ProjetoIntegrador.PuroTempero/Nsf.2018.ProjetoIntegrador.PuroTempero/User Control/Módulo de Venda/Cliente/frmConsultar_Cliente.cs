﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Cliente
{
    public partial class frmConsultar_Cliente : UserControl
    {
        public frmConsultar_Cliente()
        {
            InitializeComponent();
            PermitirAcesso();
        }

        private void PermitirAcesso()
        {
            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }
        }
        private void CarregarGrid()
        {
            DTO_Cliente dto = new DTO_Cliente();
            dto.Nome = txtNome.Text;

            Business_Cliente db = new Business_Cliente();
            List<DTO_Cliente> clientes = db.Consultar(dto);

            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = clientes;
        }

        private void gpbBuscar_Paint_1(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);


                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGrid();
        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                DTO_Cliente linha = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;

                frmAlterar_Cliente cliente = new frmAlterar_Cliente();
                cliente.LoadScreen(linha);

                frmMenu.Atual.OpenScreen(cliente);
            }
            if (e.ColumnIndex == 9)
            {
                DTO_Cliente linha = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja apagar esse registro?",
                                                      "Puro Tempero",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    Business_Cliente db = new Business_Cliente();
                    db.Remover(linha);

                    CarregarGrid();
                    txtNome.Clear();
                }
            }
        }
    }
}
