﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra
{
    public partial class frmCadastrar_PedidoVenda : UserControl
    {
        //É essa maravilha que irá fazer todo o nosso trabalho... Pois a mesma irá irá trazer todos os valores_
        //Encontrados na combobox selecionada e transforma em uma lista na griedview 
        BindingList<DTO_ProdutoVenda> produtosCarrinho = new BindingList<DTO_ProdutoVenda>();

        public frmCadastrar_PedidoVenda()
        {
            InitializeComponent();

            //Carregando os métodos
            CarregarCombo();
            CarregarGrid();
        }

        //Método para carregar o valor da gried sempre que for alterado
        private void CarregarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho; 
        }

        //Método para carregar o valor encontrado no método de listar
        private void CarregarCombo()
        {
            Business_ProdutoVenda db = new Business_ProdutoVenda();
            List<DTO_ProdutoVenda> produtos = db.Listar();

            cboProduto.ValueMember = nameof(DTO_ProdutoVenda.ID);
            cboProduto.DisplayMember = nameof(DTO_ProdutoVenda.Produto);

            cboProduto.DataSource = produtos;

            // ======================================================= //

            Business_Cliente banco = new Business_Cliente();
            List<DTO_Cliente> empresas = banco.Listar();

            cboCliente.ValueMember = nameof(DTO_Cliente.ID_Cliente);
            cboCliente.DisplayMember = nameof(DTO_Cliente.Nome);

            cboCliente.DataSource = empresas;
        }

        private void gbpProduto_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnConcluir_Click(object sender, EventArgs e)
        {
            //Pegando o valor da combo
            DTO_Cliente cliente = cboCliente.SelectedItem as DTO_Cliente;
            DTO_Vendas dto = new DTO_Vendas();

            //Pegando os valores dos controles
            dto.Data = dtpPedido.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            dto.ID_Cliente = cliente.ID_Cliente;

            Business_Vendas db = new Business_Vendas();
            db.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso!!", 
                            "Puro Tempero",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void btnVoltar_Click_1(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void lblAdicionar_Click_1(object sender, EventArgs e)
        {
            DTO_ProdutoVenda dto = cboProduto.SelectedItem as DTO_ProdutoVenda;
            int qtd = Convert.ToInt32(nudQuantidade.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        //Evento que move os valores
        private void cboProduto_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //Pego o valor selecionado no combo
            DTO_ProdutoVenda produto = cboProduto.SelectedItem as DTO_ProdutoVenda;

            //Carrego a label de preço com o valor equivalente na combo
            lblPreco.Text = "R$: " + produto.Preco.ToString();

            //Faço uma conta do valor total
            decimal total = produto.Preco * nudQuantidade.Value;

            //Carrego a label de total com o valor equivalente na combo, não esquecendo de pegar a variável com o valor acima
            lblTotal.Text = "R$: " + total.ToString();

            //Carrego a picture box com o valor equivalente na combo
            imgProduto.Image = ImagemPlugin.ConverterParaImagem(produto.Imagem);
        }

        //Semelhante ao evento acima da combo... O mesmo traz o valor equivalente ao número que o mesmo está pedindo
        private void nudQuantidade_ValueChanged_1(object sender, EventArgs e)
        {
            //Pego o valor selecionado no combo
            DTO_ProdutoVenda produto = cboProduto.SelectedItem as DTO_ProdutoVenda;

            //Faço uma conta do valor total
            decimal total = produto.Preco * nudQuantidade.Value;

            //Carrego a label de total com o valor equivalente na combo, não esquecendo de pegar a variável com o valor acima
            lblTotal.Text = "R$: " + total.ToString();
        }
    }
}
