﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra
{
    partial class frmCadastrar_PedidoVenda
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbCadastrarPedido = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblAdicionar = new System.Windows.Forms.Button();
            this.dgvItens = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.pnlFornecedor = new System.Windows.Forms.Panel();
            this.lblPedido = new System.Windows.Forms.Label();
            this.lblFornecedor = new System.Windows.Forms.Label();
            this.dtpPedido = new System.Windows.Forms.DateTimePicker();
            this.cboCliente = new System.Windows.Forms.ComboBox();
            this.pnlProdutos = new System.Windows.Forms.Panel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblT = new System.Windows.Forms.Label();
            this.lblPreco = new System.Windows.Forms.Label();
            this.lblP = new System.Windows.Forms.Label();
            this.nudQuantidade = new System.Windows.Forms.NumericUpDown();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lblProduto = new System.Windows.Forms.Label();
            this.cboProduto = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgProduto = new System.Windows.Forms.PictureBox();
            this.btnConcluir = new System.Windows.Forms.Button();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbCadastrarPedido.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItens)).BeginInit();
            this.pnlFornecedor.SuspendLayout();
            this.pnlProdutos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbCadastrarPedido
            // 
            this.gpbCadastrarPedido.BackColor = System.Drawing.Color.Transparent;
            this.gpbCadastrarPedido.Controls.Add(this.panel4);
            this.gpbCadastrarPedido.Controls.Add(this.pnlFornecedor);
            this.gpbCadastrarPedido.Controls.Add(this.pnlProdutos);
            this.gpbCadastrarPedido.Controls.Add(this.panel1);
            this.gpbCadastrarPedido.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbCadastrarPedido.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbCadastrarPedido.Location = new System.Drawing.Point(11, 3);
            this.gpbCadastrarPedido.Name = "gpbCadastrarPedido";
            this.gpbCadastrarPedido.Size = new System.Drawing.Size(586, 394);
            this.gpbCadastrarPedido.TabIndex = 21;
            this.gpbCadastrarPedido.TabStop = false;
            this.gpbCadastrarPedido.Text = "Pedido de Venda";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.lblAdicionar);
            this.panel4.Controls.Add(this.dgvItens);
            this.panel4.Location = new System.Drawing.Point(5, 201);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(576, 177);
            this.panel4.TabIndex = 41;
            // 
            // lblAdicionar
            // 
            this.lblAdicionar.BackColor = System.Drawing.Color.DarkGreen;
            this.lblAdicionar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAdicionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblAdicionar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdicionar.ForeColor = System.Drawing.Color.White;
            this.lblAdicionar.Location = new System.Drawing.Point(5, 3);
            this.lblAdicionar.Name = "lblAdicionar";
            this.lblAdicionar.Size = new System.Drawing.Size(23, 27);
            this.lblAdicionar.TabIndex = 13;
            this.lblAdicionar.Tag = "";
            this.lblAdicionar.Text = "+";
            this.lblAdicionar.UseVisualStyleBackColor = false;
            this.lblAdicionar.Click += new System.EventHandler(this.lblAdicionar_Click_1);
            // 
            // dgvItens
            // 
            this.dgvItens.AllowUserToAddRows = false;
            this.dgvItens.AllowUserToDeleteRows = false;
            this.dgvItens.BackgroundColor = System.Drawing.Color.White;
            this.dgvItens.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvItens.ColumnHeadersHeight = 30;
            this.dgvItens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Preco,
            this.Column3});
            this.dgvItens.Location = new System.Drawing.Point(6, 36);
            this.dgvItens.Name = "dgvItens";
            this.dgvItens.ReadOnly = true;
            this.dgvItens.RowHeadersVisible = false;
            this.dgvItens.Size = new System.Drawing.Size(560, 128);
            this.dgvItens.TabIndex = 12;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "Produto";
            this.Column2.HeaderText = "Produto";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Preco
            // 
            this.Preco.DataPropertyName = "Preco";
            this.Preco.HeaderText = "Preço";
            this.Preco.Name = "Preco";
            this.Preco.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Remove;
            this.Column3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            this.Column3.Width = 25;
            // 
            // pnlFornecedor
            // 
            this.pnlFornecedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlFornecedor.Controls.Add(this.lblPedido);
            this.pnlFornecedor.Controls.Add(this.lblFornecedor);
            this.pnlFornecedor.Controls.Add(this.dtpPedido);
            this.pnlFornecedor.Controls.Add(this.cboCliente);
            this.pnlFornecedor.Location = new System.Drawing.Point(5, 122);
            this.pnlFornecedor.Name = "pnlFornecedor";
            this.pnlFornecedor.Size = new System.Drawing.Size(472, 73);
            this.pnlFornecedor.TabIndex = 40;
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.BackColor = System.Drawing.Color.Transparent;
            this.lblPedido.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedido.ForeColor = System.Drawing.Color.Black;
            this.lblPedido.Location = new System.Drawing.Point(245, 14);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(108, 20);
            this.lblPedido.TabIndex = 42;
            this.lblPedido.Text = "Data do Pedido:";
            // 
            // lblFornecedor
            // 
            this.lblFornecedor.AutoSize = true;
            this.lblFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.lblFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedor.ForeColor = System.Drawing.Color.Black;
            this.lblFornecedor.Location = new System.Drawing.Point(9, 14);
            this.lblFornecedor.Name = "lblFornecedor";
            this.lblFornecedor.Size = new System.Drawing.Size(60, 20);
            this.lblFornecedor.TabIndex = 41;
            this.lblFornecedor.Text = "Cliente: ";
            // 
            // dtpPedido
            // 
            this.dtpPedido.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPedido.Location = new System.Drawing.Point(249, 36);
            this.dtpPedido.Name = "dtpPedido";
            this.dtpPedido.Size = new System.Drawing.Size(211, 22);
            this.dtpPedido.TabIndex = 39;
            // 
            // cboCliente
            // 
            this.cboCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCliente.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCliente.FormattingEnabled = true;
            this.cboCliente.Location = new System.Drawing.Point(13, 34);
            this.cboCliente.Name = "cboCliente";
            this.cboCliente.Size = new System.Drawing.Size(214, 24);
            this.cboCliente.TabIndex = 40;
            // 
            // pnlProdutos
            // 
            this.pnlProdutos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlProdutos.Controls.Add(this.lblTotal);
            this.pnlProdutos.Controls.Add(this.lblT);
            this.pnlProdutos.Controls.Add(this.lblPreco);
            this.pnlProdutos.Controls.Add(this.lblP);
            this.pnlProdutos.Controls.Add(this.nudQuantidade);
            this.pnlProdutos.Controls.Add(this.lblQuantidade);
            this.pnlProdutos.Controls.Add(this.lblProduto);
            this.pnlProdutos.Controls.Add(this.cboProduto);
            this.pnlProdutos.Location = new System.Drawing.Point(5, 25);
            this.pnlProdutos.Name = "pnlProdutos";
            this.pnlProdutos.Size = new System.Drawing.Size(472, 94);
            this.pnlProdutos.TabIndex = 39;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(306, 59);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(58, 20);
            this.lblTotal.TabIndex = 25;
            this.lblTotal.Text = "R$: 0,00";
            // 
            // lblT
            // 
            this.lblT.AutoSize = true;
            this.lblT.BackColor = System.Drawing.Color.Transparent;
            this.lblT.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblT.ForeColor = System.Drawing.Color.Black;
            this.lblT.Location = new System.Drawing.Point(244, 58);
            this.lblT.Name = "lblT";
            this.lblT.Size = new System.Drawing.Size(44, 20);
            this.lblT.TabIndex = 26;
            this.lblT.Text = "Total:";
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.ForeColor = System.Drawing.Color.Black;
            this.lblPreco.Location = new System.Drawing.Point(77, 59);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(58, 20);
            this.lblPreco.TabIndex = 23;
            this.lblPreco.Text = "R$: 0,00";
            // 
            // lblP
            // 
            this.lblP.AutoSize = true;
            this.lblP.BackColor = System.Drawing.Color.Transparent;
            this.lblP.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP.ForeColor = System.Drawing.Color.Black;
            this.lblP.Location = new System.Drawing.Point(9, 58);
            this.lblP.Name = "lblP";
            this.lblP.Size = new System.Drawing.Size(49, 20);
            this.lblP.TabIndex = 24;
            this.lblP.Text = "Preço:";
            // 
            // nudQuantidade
            // 
            this.nudQuantidade.Font = new System.Drawing.Font("Arial Narrow", 10F, System.Drawing.FontStyle.Bold);
            this.nudQuantidade.Location = new System.Drawing.Point(249, 33);
            this.nudQuantidade.Name = "nudQuantidade";
            this.nudQuantidade.Size = new System.Drawing.Size(206, 23);
            this.nudQuantidade.TabIndex = 22;
            this.nudQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudQuantidade.ValueChanged += new System.EventHandler(this.nudQuantidade_ValueChanged_1);
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantidade.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.Color.Black;
            this.lblQuantidade.Location = new System.Drawing.Point(243, 13);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(80, 20);
            this.lblQuantidade.TabIndex = 21;
            this.lblQuantidade.Text = "Quantidade";
            // 
            // lblProduto
            // 
            this.lblProduto.AutoSize = true;
            this.lblProduto.BackColor = System.Drawing.Color.Transparent;
            this.lblProduto.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.ForeColor = System.Drawing.Color.Black;
            this.lblProduto.Location = new System.Drawing.Point(9, 13);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(59, 20);
            this.lblProduto.TabIndex = 20;
            this.lblProduto.Text = "Produto";
            // 
            // cboProduto
            // 
            this.cboProduto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProduto.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProduto.FormattingEnabled = true;
            this.cboProduto.Location = new System.Drawing.Point(13, 33);
            this.cboProduto.Name = "cboProduto";
            this.cboProduto.Size = new System.Drawing.Size(220, 24);
            this.cboProduto.TabIndex = 19;
            this.cboProduto.SelectedIndexChanged += new System.EventHandler(this.cboProduto_SelectedIndexChanged_1);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.imgProduto);
            this.panel1.Location = new System.Drawing.Point(481, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 170);
            this.panel1.TabIndex = 15;
            // 
            // imgProduto
            // 
            this.imgProduto.BackColor = System.Drawing.Color.White;
            this.imgProduto.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Tacos;
            this.imgProduto.Location = new System.Drawing.Point(4, 7);
            this.imgProduto.Name = "imgProduto";
            this.imgProduto.Size = new System.Drawing.Size(87, 151);
            this.imgProduto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgProduto.TabIndex = 14;
            this.imgProduto.TabStop = false;
            // 
            // btnConcluir
            // 
            this.btnConcluir.BackColor = System.Drawing.Color.DarkGreen;
            this.btnConcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConcluir.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConcluir.ForeColor = System.Drawing.Color.White;
            this.btnConcluir.Location = new System.Drawing.Point(469, 401);
            this.btnConcluir.Name = "btnConcluir";
            this.btnConcluir.Size = new System.Drawing.Size(128, 30);
            this.btnConcluir.TabIndex = 11;
            this.btnConcluir.Tag = "";
            this.btnConcluir.Text = "Concluir Venda";
            this.btnConcluir.UseVisualStyleBackColor = false;
            this.btnConcluir.Click += new System.EventHandler(this.btnConcluir_Click);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(347, 403);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(116, 27);
            this.btnVoltar.TabIndex = 33;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click_1);
            // 
            // frmCadastrar_PedidoVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.gpbCadastrarPedido);
            this.Controls.Add(this.btnConcluir);
            this.Name = "frmCadastrar_PedidoVenda";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbCadastrarPedido.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItens)).EndInit();
            this.pnlFornecedor.ResumeLayout(false);
            this.pnlFornecedor.PerformLayout();
            this.pnlProdutos.ResumeLayout(false);
            this.pnlProdutos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbCadastrarPedido;
        private System.Windows.Forms.Button btnConcluir;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox imgProduto;
        private System.Windows.Forms.Panel pnlFornecedor;
        private System.Windows.Forms.Panel pnlProdutos;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.Label lblFornecedor;
        private System.Windows.Forms.ComboBox cboCliente;
        private System.Windows.Forms.DateTimePicker dtpPedido;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button lblAdicionar;
        private System.Windows.Forms.DataGridView dgvItens;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblT;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.Label lblP;
        private System.Windows.Forms.NumericUpDown nudQuantidade;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.ComboBox cboProduto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preco;
        private System.Windows.Forms.DataGridViewImageColumn Column3;
    }
}
