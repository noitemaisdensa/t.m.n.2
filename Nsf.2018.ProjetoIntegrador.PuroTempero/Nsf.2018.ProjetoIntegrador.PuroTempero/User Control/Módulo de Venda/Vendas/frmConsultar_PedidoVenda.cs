﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf.PuroTempero.DATABASE.Módulo_Compras;
using Nsf.PuroTempero.MODELO.Módulo_Compras;
using Nsf.PuroTempero.MODELO.Módulo_Venda;
using Nsf.PuroTempero.DATABASE.Módulo_Venda;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra
{
    public partial class frmConsultar_PedidoVenda : UserControl
    {
        public frmConsultar_PedidoVenda()
        {
            InitializeComponent();
        }

        private void CarregarGrid ()
        {
            VIEW_Venda db = new VIEW_Venda();
            List<VIEW_VendaDTO> list = db.Consultar(dtpInicio.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59), dtpFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59));

            dgvCompra.AutoGenerateColumns = false;
            dgvCompra.DataSource = list;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void gpbBusca_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }


        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
