﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Compra
{
    partial class frmConsultar_PedidoVenda
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbBusca = new System.Windows.Forms.GroupBox();
            this.dgvCompra = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpFim = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.lblFim = new System.Windows.Forms.Label();
            this.lblInicio = new System.Windows.Forms.Label();
            this.gpbInformacoes = new System.Windows.Forms.GroupBox();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.rtxtOBS = new System.Windows.Forms.RichTextBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbBusca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).BeginInit();
            this.gpbInformacoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbBusca
            // 
            this.gpbBusca.Controls.Add(this.dgvCompra);
            this.gpbBusca.Controls.Add(this.dtpFim);
            this.gpbBusca.Controls.Add(this.dtpInicio);
            this.gpbBusca.Controls.Add(this.btnConsultar);
            this.gpbBusca.Controls.Add(this.lblFim);
            this.gpbBusca.Controls.Add(this.lblInicio);
            this.gpbBusca.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbBusca.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbBusca.Location = new System.Drawing.Point(18, 3);
            this.gpbBusca.Name = "gpbBusca";
            this.gpbBusca.Size = new System.Drawing.Size(424, 390);
            this.gpbBusca.TabIndex = 31;
            this.gpbBusca.TabStop = false;
            this.gpbBusca.Text = "Busca ";
            this.gpbBusca.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBusca_Paint);
            // 
            // dgvCompra
            // 
            this.dgvCompra.AllowUserToAddRows = false;
            this.dgvCompra.AllowUserToDeleteRows = false;
            this.dgvCompra.AllowUserToResizeColumns = false;
            this.dgvCompra.AllowUserToResizeRows = false;
            this.dgvCompra.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvCompra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCompra.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgvCompra.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompra.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column4});
            this.dgvCompra.GridColor = System.Drawing.Color.Snow;
            this.dgvCompra.Location = new System.Drawing.Point(10, 168);
            this.dgvCompra.MultiSelect = false;
            this.dgvCompra.Name = "dgvCompra";
            this.dgvCompra.ReadOnly = true;
            this.dgvCompra.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvCompra.RowHeadersVisible = false;
            this.dgvCompra.Size = new System.Drawing.Size(404, 210);
            this.dgvCompra.TabIndex = 27;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ID";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 45;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Data";
            this.Column2.HeaderText = "Data";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 90;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Cliente";
            this.Column3.HeaderText = "Cliente";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Quantidade";
            this.Column5.HeaderText = "Itens";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Total";
            this.Column4.HeaderText = "Total";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // dtpFim
            // 
            this.dtpFim.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFim.Location = new System.Drawing.Point(30, 93);
            this.dtpFim.Name = "dtpFim";
            this.dtpFim.Size = new System.Drawing.Size(291, 22);
            this.dtpFim.TabIndex = 26;
            // 
            // dtpInicio
            // 
            this.dtpInicio.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicio.Location = new System.Drawing.Point(30, 48);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(291, 22);
            this.dtpInicio.TabIndex = 25;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.Black;
            this.btnConsultar.Location = new System.Drawing.Point(322, 133);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(92, 27);
            this.btnConsultar.TabIndex = 3;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // lblFim
            // 
            this.lblFim.AutoSize = true;
            this.lblFim.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFim.ForeColor = System.Drawing.Color.Black;
            this.lblFim.Location = new System.Drawing.Point(26, 74);
            this.lblFim.Name = "lblFim";
            this.lblFim.Size = new System.Drawing.Size(42, 21);
            this.lblFim.TabIndex = 18;
            this.lblFim.Text = "Fim:";
            // 
            // lblInicio
            // 
            this.lblInicio.AutoSize = true;
            this.lblInicio.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInicio.ForeColor = System.Drawing.Color.Black;
            this.lblInicio.Location = new System.Drawing.Point(26, 29);
            this.lblInicio.Name = "lblInicio";
            this.lblInicio.Size = new System.Drawing.Size(57, 21);
            this.lblInicio.TabIndex = 17;
            this.lblInicio.Text = "Inicio:";
            // 
            // gpbInformacoes
            // 
            this.gpbInformacoes.Controls.Add(this.imgImagem);
            this.gpbInformacoes.Controls.Add(this.lblOBS);
            this.gpbInformacoes.Controls.Add(this.rtxtOBS);
            this.gpbInformacoes.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformacoes.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformacoes.Location = new System.Drawing.Point(447, 3);
            this.gpbInformacoes.Name = "gpbInformacoes";
            this.gpbInformacoes.Size = new System.Drawing.Size(142, 390);
            this.gpbInformacoes.TabIndex = 30;
            this.gpbInformacoes.TabStop = false;
            this.gpbInformacoes.Text = "Informações ";
            this.gpbInformacoes.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBusca_Paint);
            // 
            // imgImagem
            // 
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Pesquisa1;
            this.imgImagem.Location = new System.Drawing.Point(10, 37);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(126, 180);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(6, 243);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 28;
            this.lblOBS.Text = "OBS:";
            // 
            // rtxtOBS
            // 
            this.rtxtOBS.Enabled = false;
            this.rtxtOBS.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtOBS.Location = new System.Drawing.Point(6, 263);
            this.rtxtOBS.Name = "rtxtOBS";
            this.rtxtOBS.Size = new System.Drawing.Size(130, 115);
            this.rtxtOBS.TabIndex = 27;
            this.rtxtOBS.Text = "Para fazer qualquer consulta é necessário ter privilégios definidos pelo Administ" +
    "rador.";
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(473, 399);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(116, 30);
            this.btnVoltar.TabIndex = 32;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // frmConsultar_PedidoVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbBusca);
            this.Controls.Add(this.gpbInformacoes);
            this.Controls.Add(this.btnVoltar);
            this.Name = "frmConsultar_PedidoVenda";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbBusca.ResumeLayout(false);
            this.gpbBusca.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).EndInit();
            this.gpbInformacoes.ResumeLayout(false);
            this.gpbInformacoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gpbBusca;
        private System.Windows.Forms.DateTimePicker dtpFim;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Label lblFim;
        private System.Windows.Forms.Label lblInicio;
        private System.Windows.Forms.GroupBox gpbInformacoes;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.RichTextBox rtxtOBS;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.DataGridView dgvCompra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}
