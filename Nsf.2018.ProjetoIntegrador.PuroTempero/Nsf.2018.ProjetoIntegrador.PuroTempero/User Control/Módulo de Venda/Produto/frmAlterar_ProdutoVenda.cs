﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public partial class frmAlterar_ProdutoVenda : UserControl
    {
        public frmAlterar_ProdutoVenda()
        {
            InitializeComponent();
        }

        DTO_ProdutoVenda dto;
        public void LoadScreen (DTO_ProdutoVenda dto)
        {
            this.dto = dto;

            lblID.Text = dto.ID.ToString();
            txtProduto.Text = dto.Produto;
            nudQuantidade.Value = dto.Preco;
            imgImagem.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }
        

        private void AlterarProduto (DTO_ProdutoVenda dto)
        {
            dto.Produto = txtProduto.Text;
            dto.Preco = nudQuantidade.Value;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgImagem.Image);
        }
        
        private void gbpProduto_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            AlterarProduto(dto);

            Business_ProdutoVenda db = new Business_ProdutoVenda();
            db.Alterar(dto);
            MessageBox.Show("Produto alterado com sucesso!!",
                            "Puro Tempero",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";

            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgImagem.ImageLocation = dialog.FileName;
            }
        }

        private void nudQuantidade_ValueChanged(object sender, EventArgs e)
        {
            decimal total = nudQuantidade.Value;
             
            lblValorTotal.Text = $"R$: {total}";
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu menu = new frmMenu();
            menu.Show();
        }
    }
}
