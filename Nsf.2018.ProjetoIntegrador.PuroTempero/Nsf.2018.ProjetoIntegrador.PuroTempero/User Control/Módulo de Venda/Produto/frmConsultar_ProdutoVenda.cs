﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Produto
{
    public partial class frmConsultar_ProdutoVenda : UserControl
    {
        Business_ProdutoVenda db = new Business_ProdutoVenda();

        public frmConsultar_ProdutoVenda()
        {
            InitializeComponent();
            PermitirAcesso();
        }

        private void PermitirAcesso()
        {
            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void gpbBuscar_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }


        }

        //Passo o valor carregado no controle para o método de consultar, e depois, esse valor vai para a GRIED
        private void CarregarGrid()
        {
            DTO_ProdutoVenda dto = new DTO_ProdutoVenda();
            dto.Produto = txtProduto.Text;

            List<DTO_ProdutoVenda> consult = db.Consultar(dto);

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = consult;
        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Posição 3 da gried
            if (e.ColumnIndex == 3)
            {
                //Pego o valor da linha selecionada
                DTO_ProdutoVenda linha = dgvProduto.CurrentRow.DataBoundItem as DTO_ProdutoVenda;

                //Instacio o formulário da tela de produto
                frmAlterar_ProdutoVenda tela = new frmAlterar_ProdutoVenda();

                //Chamo o Método LoadScreen passando como parâmetro o valor encontrado na linha da gried
                tela.LoadScreen(linha);

                //Chamo o método OpenScreen presente na formulário onde contém o Menu
                frmMenu.Atual.OpenScreen(tela);

                //Oculto essa tela
                this.Hide();
            }
            //Posição 4 da gried
            if (e.ColumnIndex == 4)
            {
                //Pego o valor da linha selecionada
                DTO_ProdutoVenda linha = dgvProduto.CurrentRow.DataBoundItem as DTO_ProdutoVenda;

                //Mensagem para o Usuário
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja apagar esse registro?",
                                                      "AVISO!!",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    //Chamo o método Remover e passo o valor do ID
                    db.Remover(linha.ID);

                    //Carrego a gried.
                    CarregarGrid();
                }

            }
        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGrid();
        }
    }
}
