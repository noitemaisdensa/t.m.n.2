﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário;
using static System.Net.Mime.MediaTypeNames;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Localização;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.DTO;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;
using Nsf.PuroTempero.MODELO.Módulo_Acesso;
using Nsf.PuroTempero.BUSINESS.Módulo_Acesso;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    public partial class frmSalvar_Funcionario : UserControl
    {
        public frmSalvar_Funcionario()
        {
            InitializeComponent();
            CarregarComboBox();
        }
        void CarregarComboBox()
        {
            Localizacao localizacao = new Localizacao();
            List<string> lista = new List<string>();
            lista = localizacao.UF();
            cboUF.DisplayMember = lista.ToString();
            cboUF.DataSource = lista;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void gpbAcesso_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void gpbDadosPessoais_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void gpbVendas_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void gpbAcesso_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void groupBox1_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                // Salvar Funcionário


                DTO_Funcionario dto = new DTO_Funcionario();
                dto.Nome = txtNome.Text;

                // if para n salvar RG só com a Mask (Não mexer para n afetar outra parte)
                if (txtRG.Text == "  .   .   -")
                {
                    txtRG.Text = null;
                }
                else
                {
                    dto.RG = txtRG.Text;
                }
                // if para n salvar CPF só com a Mask (Não mexer para n afetar outra parte)


                if (txtCPF.Text == "         /")
                {
                    txtCPF.Text = null;
                }
                else
                {
                    dto.CPF = txtCPF.Text;
                }

                dto.Nascimento = dtpNascimento.Value;
                if (chkF.Checked == true)
                {
                    dto.Sexo = chkF.Text.ToString();
                }
                if (chkM.Checked == true)
                {
                    dto.Sexo = chkM.Text.ToString();
                }
                dto.Transporte = nudVT.Value;
                dto.Refeicao = nudVR.Value;
                dto.Alimentacao = nudVA.Value;
                dto.Convenio = nudConvenio.Value;
                dto.Salario = nudSalario.Value;
                dto.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                dto.Usuario = txtUsuario.Text;
                dto.Senha = txtSenha.Text.Trim();
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Cidade = txtCidade.Text;
                dto.Endereço = txtEndereco.Text;
                dto.Numero = Convert.ToInt32(nudNumero.Value);
                dto.CEP = txtCEP.Text;
                dto.Complemento = txtComplemento.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                Business_Funcionario business = new Business_Funcionario();
                int id_funcionario = business.Salvar(dto);
                MessageBox.Show("Salvo com Sucesso!", "Puro-Tempero", MessageBoxButtons.OK, MessageBoxIcon.Information);


                // Salva Permissões na Tabela Nivel de Acesso

                //Checka Permissões de Vendas

                if (chkSalvarVendas.Checked == true || chkRemoverVendas.Checked == true || chkConsultarVendas.Checked == true || chkAlterarVendas.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "Vendas";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarVendas.Checked;
                    dt.Remover = chkRemoverVendas.Checked;
                    dt.Consultar = chkConsultarVendas.Checked;
                    dt.Alterar = chkAlterarVendas.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka permissões em Funcionários
                if (chkSalvarFuncionario.Checked == true || chkRemoverFuncionario.Checked == true || chkConsultarFuncionario.Checked == true || chkAlterarFuncionario.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "Funcionários";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarFuncionario.Checked;
                    dt.Remover = chkRemoverFuncionario.Checked;
                    dt.Consultar = chkConsultarFuncionario.Checked;
                    dt.Alterar = chkAlterarFuncionario.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de RH
                if (chkSalvarRH.Checked == true || chkRemoverRH.Checked == true || chkConsultarRH.Checked == true || chkAlterarRH.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "RH";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarRH.Checked;
                    dt.Remover = chkRemoverRH.Checked;
                    dt.Consultar = chkConsultarRH.Checked;
                    dt.Alterar = chkAlterarRH.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões do Financeiro
                if (chkSalvarFinanceiro.Checked == true || chkRemoverFinanceiro.Checked == true || chkConsultarFinanceiro.Checked == true || chkAlterarFinanceiro.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "Financeiro";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarFinanceiro.Checked;
                    dt.Remover = chkRemoverFinanceiro.Checked;
                    dt.Consultar = chkConsultarFinanceiro.Checked;
                    dt.Alterar = chkAlterarFinanceiro.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de Compras
                if (chkSalvarCompras.Checked == true || chkRemoverCompras.Checked == true || chkConsultarCompras.Checked == true || chkAlterarCompras.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "Compras";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarCompras.Checked;
                    dt.Remover = chkRemoverCompras.Checked;
                    dt.Consultar = chkConsultarCompras.Checked;
                    dt.Alterar = chkAlterarCompras.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de Estoque
                if (chkSalvarEstoque.Checked == true || chkRemoverEstoque.Checked == true || chkConsultarEstoque.Checked == true || chkAlterarEstoque.Checked == true)
                {

                    DTO_Departamento deteo = new DTO_Departamento();
                    deteo.Departamento = "Estoque";
                    // Pesquisa departamento e volta o id

                    Database_Departamento departamento = new Database_Departamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarEstoque.Checked;
                    dt.Remover = chkRemoverEstoque.Checked;
                    dt.Consultar = chkConsultarEstoque.Checked;
                    dt.Alterar = chkAlterarEstoque.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnVoltar_Click_2(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {

        }
    }
}
