﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    partial class frmSalvar_Funcionario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tpPermissao = new System.Windows.Forms.TabPage();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbVendas = new System.Windows.Forms.GroupBox();
            this.chkConsultarVendas = new System.Windows.Forms.CheckBox();
            this.chkSalvarVendas = new System.Windows.Forms.CheckBox();
            this.chkAlterarVendas = new System.Windows.Forms.CheckBox();
            this.chkRemoverVendas = new System.Windows.Forms.CheckBox();
            this.gpbAcesso = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.gpbEstoque = new System.Windows.Forms.GroupBox();
            this.chkConsultarEstoque = new System.Windows.Forms.CheckBox();
            this.chkSalvarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarEstoque = new System.Windows.Forms.CheckBox();
            this.chkRemoverEstoque = new System.Windows.Forms.CheckBox();
            this.gpbCompras = new System.Windows.Forms.GroupBox();
            this.chkConsultarCompras = new System.Windows.Forms.CheckBox();
            this.chkSalvarCompras = new System.Windows.Forms.CheckBox();
            this.chkAlterarCompras = new System.Windows.Forms.CheckBox();
            this.chkRemoverCompras = new System.Windows.Forms.CheckBox();
            this.gpbFinanceiro = new System.Windows.Forms.GroupBox();
            this.chkConsultarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkSalvarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkAlterarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRemoverFinanceiro = new System.Windows.Forms.CheckBox();
            this.gpbRH = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.chkConsultarRH = new System.Windows.Forms.CheckBox();
            this.chkSalvarRH = new System.Windows.Forms.CheckBox();
            this.chkAlterarRH = new System.Windows.Forms.CheckBox();
            this.chkRemoverRH = new System.Windows.Forms.CheckBox();
            this.gpbFuncionario = new System.Windows.Forms.GroupBox();
            this.chkConsultarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkSalvarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkRemoverFuncionario = new System.Windows.Forms.CheckBox();
            this.tbConteudo = new System.Windows.Forms.TabControl();
            this.tpFuncionario = new System.Windows.Forms.TabPage();
            this.gpbDadosPessoais = new System.Windows.Forms.GroupBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblCelular = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.imgFuncionario = new System.Windows.Forms.PictureBox();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblDataNascimento = new System.Windows.Forms.Label();
            this.lblRG = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.gpbSalario = new System.Windows.Forms.GroupBox();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.nudVT = new System.Windows.Forms.NumericUpDown();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.lblVA = new System.Windows.Forms.Label();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.lblCifrao5 = new System.Windows.Forms.Label();
            this.lblCifrao4 = new System.Windows.Forms.Label();
            this.lblCifrao3 = new System.Windows.Forms.Label();
            this.lblCifrao2 = new System.Windows.Forms.Label();
            this.lblCifrao = new System.Windows.Forms.Label();
            this.lblVT = new System.Windows.Forms.Label();
            this.lblConvenio = new System.Windows.Forms.Label();
            this.lblSalario = new System.Windows.Forms.Label();
            this.lblVR = new System.Windows.Forms.Label();
            this.gpbEndereco = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.imagePesquisa = new System.Windows.Forms.PictureBox();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.lblBairro = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.tpPermissao.SuspendLayout();
            this.gpbVendas.SuspendLayout();
            this.gpbAcesso.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gpbEstoque.SuspendLayout();
            this.gpbCompras.SuspendLayout();
            this.gpbFinanceiro.SuspendLayout();
            this.gpbRH.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gpbFuncionario.SuspendLayout();
            this.tbConteudo.SuspendLayout();
            this.tpFuncionario.SuspendLayout();
            this.gpbDadosPessoais.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).BeginInit();
            this.gpbSalario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.gpbEndereco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            this.SuspendLayout();
            // 
            // tpPermissao
            // 
            this.tpPermissao.Controls.Add(this.btnSalvar);
            this.tpPermissao.Controls.Add(this.btnVoltar);
            this.tpPermissao.Controls.Add(this.gpbVendas);
            this.tpPermissao.Controls.Add(this.gpbAcesso);
            this.tpPermissao.Location = new System.Drawing.Point(4, 25);
            this.tpPermissao.Name = "tpPermissao";
            this.tpPermissao.Padding = new System.Windows.Forms.Padding(3);
            this.tpPermissao.Size = new System.Drawing.Size(596, 408);
            this.tpPermissao.TabIndex = 1;
            this.tpPermissao.Text = "Permissão";
            this.tpPermissao.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.DarkGreen;
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(515, 374);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 28);
            this.btnSalvar.TabIndex = 10;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click_1);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(434, 374);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(75, 28);
            this.btnVoltar.TabIndex = 9;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click_2);
            // 
            // gpbVendas
            // 
            this.gpbVendas.Controls.Add(this.chkConsultarVendas);
            this.gpbVendas.Controls.Add(this.chkSalvarVendas);
            this.gpbVendas.Controls.Add(this.chkAlterarVendas);
            this.gpbVendas.Controls.Add(this.chkRemoverVendas);
            this.gpbVendas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.gpbVendas.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbVendas.Location = new System.Drawing.Point(11, 102);
            this.gpbVendas.Name = "gpbVendas";
            this.gpbVendas.Size = new System.Drawing.Size(187, 100);
            this.gpbVendas.TabIndex = 1;
            this.gpbVendas.TabStop = false;
            this.gpbVendas.Text = "Vendas ";
            this.gpbVendas.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // chkConsultarVendas
            // 
            this.chkConsultarVendas.AutoSize = true;
            this.chkConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarVendas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarVendas.Location = new System.Drawing.Point(7, 35);
            this.chkConsultarVendas.Name = "chkConsultarVendas";
            this.chkConsultarVendas.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarVendas.TabIndex = 0;
            this.chkConsultarVendas.Text = "Consultar";
            this.chkConsultarVendas.UseVisualStyleBackColor = true;
            // 
            // chkSalvarVendas
            // 
            this.chkSalvarVendas.AutoSize = true;
            this.chkSalvarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarVendas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarVendas.Location = new System.Drawing.Point(107, 35);
            this.chkSalvarVendas.Name = "chkSalvarVendas";
            this.chkSalvarVendas.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarVendas.TabIndex = 1;
            this.chkSalvarVendas.Text = "Salvar";
            this.chkSalvarVendas.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVendas
            // 
            this.chkAlterarVendas.AutoSize = true;
            this.chkAlterarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVendas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarVendas.Location = new System.Drawing.Point(7, 64);
            this.chkAlterarVendas.Name = "chkAlterarVendas";
            this.chkAlterarVendas.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarVendas.TabIndex = 2;
            this.chkAlterarVendas.Text = "Alterar";
            this.chkAlterarVendas.UseVisualStyleBackColor = true;
            // 
            // chkRemoverVendas
            // 
            this.chkRemoverVendas.AutoSize = true;
            this.chkRemoverVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverVendas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverVendas.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverVendas.Location = new System.Drawing.Point(86, 63);
            this.chkRemoverVendas.Name = "chkRemoverVendas";
            this.chkRemoverVendas.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverVendas.TabIndex = 3;
            this.chkRemoverVendas.Text = "Remover";
            this.chkRemoverVendas.UseVisualStyleBackColor = true;
            // 
            // gpbAcesso
            // 
            this.gpbAcesso.Controls.Add(this.groupBox1);
            this.gpbAcesso.Controls.Add(this.gpbEstoque);
            this.gpbAcesso.Controls.Add(this.gpbCompras);
            this.gpbAcesso.Controls.Add(this.gpbFinanceiro);
            this.gpbAcesso.Controls.Add(this.gpbRH);
            this.gpbAcesso.Controls.Add(this.gpbFuncionario);
            this.gpbAcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbAcesso.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbAcesso.Location = new System.Drawing.Point(3, -3);
            this.gpbAcesso.Name = "gpbAcesso";
            this.gpbAcesso.Size = new System.Drawing.Size(590, 332);
            this.gpbAcesso.TabIndex = 0;
            this.gpbAcesso.TabStop = false;
            this.gpbAcesso.Text = "Acesso ";
            this.gpbAcesso.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbAcesso_Paint);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtUsuario);
            this.groupBox1.Controls.Add(this.txtSenha);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Controls.Add(this.lblSenha);
            this.groupBox1.Location = new System.Drawing.Point(179, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 74);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            this.groupBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBox1_Paint);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(73, 19);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(138, 20);
            this.txtUsuario.TabIndex = 3;
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(73, 45);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(137, 20);
            this.txtSenha.TabIndex = 4;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblUsuario.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario.Location = new System.Drawing.Point(11, 16);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(64, 19);
            this.lblUsuario.TabIndex = 5;
            this.lblUsuario.Text = "Usuário:";
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblSenha.ForeColor = System.Drawing.Color.Black;
            this.lblSenha.Location = new System.Drawing.Point(11, 46);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(53, 19);
            this.lblSenha.TabIndex = 6;
            this.lblSenha.Text = "Senha:";
            // 
            // gpbEstoque
            // 
            this.gpbEstoque.Controls.Add(this.chkConsultarEstoque);
            this.gpbEstoque.Controls.Add(this.chkSalvarEstoque);
            this.gpbEstoque.Controls.Add(this.chkAlterarEstoque);
            this.gpbEstoque.Controls.Add(this.chkRemoverEstoque);
            this.gpbEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEstoque.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbEstoque.Location = new System.Drawing.Point(398, 211);
            this.gpbEstoque.Name = "gpbEstoque";
            this.gpbEstoque.Size = new System.Drawing.Size(186, 100);
            this.gpbEstoque.TabIndex = 6;
            this.gpbEstoque.TabStop = false;
            this.gpbEstoque.Text = "Estoque ";
            this.gpbEstoque.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // chkConsultarEstoque
            // 
            this.chkConsultarEstoque.AutoSize = true;
            this.chkConsultarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarEstoque.Location = new System.Drawing.Point(12, 35);
            this.chkConsultarEstoque.Name = "chkConsultarEstoque";
            this.chkConsultarEstoque.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarEstoque.TabIndex = 0;
            this.chkConsultarEstoque.Text = "Consultar";
            this.chkConsultarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkSalvarEstoque
            // 
            this.chkSalvarEstoque.AutoSize = true;
            this.chkSalvarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarEstoque.Location = new System.Drawing.Point(113, 35);
            this.chkSalvarEstoque.Name = "chkSalvarEstoque";
            this.chkSalvarEstoque.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarEstoque.TabIndex = 1;
            this.chkSalvarEstoque.Text = "Salvar";
            this.chkSalvarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarEstoque
            // 
            this.chkAlterarEstoque.AutoSize = true;
            this.chkAlterarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarEstoque.Location = new System.Drawing.Point(12, 64);
            this.chkAlterarEstoque.Name = "chkAlterarEstoque";
            this.chkAlterarEstoque.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarEstoque.TabIndex = 2;
            this.chkAlterarEstoque.Text = "Alterar";
            this.chkAlterarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkRemoverEstoque
            // 
            this.chkRemoverEstoque.AutoSize = true;
            this.chkRemoverEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverEstoque.Location = new System.Drawing.Point(98, 64);
            this.chkRemoverEstoque.Name = "chkRemoverEstoque";
            this.chkRemoverEstoque.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverEstoque.TabIndex = 3;
            this.chkRemoverEstoque.Text = "Remover";
            this.chkRemoverEstoque.UseVisualStyleBackColor = true;
            // 
            // gpbCompras
            // 
            this.gpbCompras.Controls.Add(this.chkConsultarCompras);
            this.gpbCompras.Controls.Add(this.chkSalvarCompras);
            this.gpbCompras.Controls.Add(this.chkAlterarCompras);
            this.gpbCompras.Controls.Add(this.chkRemoverCompras);
            this.gpbCompras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbCompras.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbCompras.Location = new System.Drawing.Point(200, 211);
            this.gpbCompras.Name = "gpbCompras";
            this.gpbCompras.Size = new System.Drawing.Size(192, 100);
            this.gpbCompras.TabIndex = 5;
            this.gpbCompras.TabStop = false;
            this.gpbCompras.Text = "Compras ";
            this.gpbCompras.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // chkConsultarCompras
            // 
            this.chkConsultarCompras.AutoSize = true;
            this.chkConsultarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarCompras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarCompras.Location = new System.Drawing.Point(6, 35);
            this.chkConsultarCompras.Name = "chkConsultarCompras";
            this.chkConsultarCompras.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarCompras.TabIndex = 0;
            this.chkConsultarCompras.Text = "Consultar";
            this.chkConsultarCompras.UseVisualStyleBackColor = true;
            // 
            // chkSalvarCompras
            // 
            this.chkSalvarCompras.AutoSize = true;
            this.chkSalvarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarCompras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarCompras.Location = new System.Drawing.Point(107, 35);
            this.chkSalvarCompras.Name = "chkSalvarCompras";
            this.chkSalvarCompras.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarCompras.TabIndex = 1;
            this.chkSalvarCompras.Text = "Salvar";
            this.chkSalvarCompras.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCompras
            // 
            this.chkAlterarCompras.AutoSize = true;
            this.chkAlterarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCompras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarCompras.Location = new System.Drawing.Point(6, 64);
            this.chkAlterarCompras.Name = "chkAlterarCompras";
            this.chkAlterarCompras.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarCompras.TabIndex = 2;
            this.chkAlterarCompras.Text = "Alterar";
            this.chkAlterarCompras.UseVisualStyleBackColor = true;
            // 
            // chkRemoverCompras
            // 
            this.chkRemoverCompras.AutoSize = true;
            this.chkRemoverCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverCompras.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverCompras.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverCompras.Location = new System.Drawing.Point(94, 64);
            this.chkRemoverCompras.Name = "chkRemoverCompras";
            this.chkRemoverCompras.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverCompras.TabIndex = 3;
            this.chkRemoverCompras.Text = "Remover";
            this.chkRemoverCompras.UseVisualStyleBackColor = true;
            // 
            // gpbFinanceiro
            // 
            this.gpbFinanceiro.Controls.Add(this.chkConsultarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkSalvarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkAlterarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkRemoverFinanceiro);
            this.gpbFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbFinanceiro.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbFinanceiro.Location = new System.Drawing.Point(7, 211);
            this.gpbFinanceiro.Name = "gpbFinanceiro";
            this.gpbFinanceiro.Size = new System.Drawing.Size(187, 100);
            this.gpbFinanceiro.TabIndex = 4;
            this.gpbFinanceiro.TabStop = false;
            this.gpbFinanceiro.Text = "Financeiro ";
            this.gpbFinanceiro.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // chkConsultarFinanceiro
            // 
            this.chkConsultarFinanceiro.AutoSize = true;
            this.chkConsultarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFinanceiro.Location = new System.Drawing.Point(12, 35);
            this.chkConsultarFinanceiro.Name = "chkConsultarFinanceiro";
            this.chkConsultarFinanceiro.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarFinanceiro.TabIndex = 0;
            this.chkConsultarFinanceiro.Text = "Consultar";
            this.chkConsultarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFinanceiro
            // 
            this.chkSalvarFinanceiro.AutoSize = true;
            this.chkSalvarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFinanceiro.Location = new System.Drawing.Point(114, 35);
            this.chkSalvarFinanceiro.Name = "chkSalvarFinanceiro";
            this.chkSalvarFinanceiro.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarFinanceiro.TabIndex = 1;
            this.chkSalvarFinanceiro.Text = "Salvar";
            this.chkSalvarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFinanceiro
            // 
            this.chkAlterarFinanceiro.AutoSize = true;
            this.chkAlterarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFinanceiro.Location = new System.Drawing.Point(12, 64);
            this.chkAlterarFinanceiro.Name = "chkAlterarFinanceiro";
            this.chkAlterarFinanceiro.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarFinanceiro.TabIndex = 2;
            this.chkAlterarFinanceiro.Text = "Alterar";
            this.chkAlterarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFinanceiro
            // 
            this.chkRemoverFinanceiro.AutoSize = true;
            this.chkRemoverFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFinanceiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFinanceiro.Location = new System.Drawing.Point(90, 63);
            this.chkRemoverFinanceiro.Name = "chkRemoverFinanceiro";
            this.chkRemoverFinanceiro.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverFinanceiro.TabIndex = 3;
            this.chkRemoverFinanceiro.Text = "Remover";
            this.chkRemoverFinanceiro.UseVisualStyleBackColor = true;
            // 
            // gpbRH
            // 
            this.gpbRH.Controls.Add(this.groupBox4);
            this.gpbRH.Controls.Add(this.chkConsultarRH);
            this.gpbRH.Controls.Add(this.chkSalvarRH);
            this.gpbRH.Controls.Add(this.chkAlterarRH);
            this.gpbRH.Controls.Add(this.chkRemoverRH);
            this.gpbRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbRH.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbRH.Location = new System.Drawing.Point(398, 105);
            this.gpbRH.Name = "gpbRH";
            this.gpbRH.Size = new System.Drawing.Size(186, 100);
            this.gpbRH.TabIndex = 3;
            this.gpbRH.TabStop = false;
            this.gpbRH.Text = "RH ";
            this.gpbRH.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBox5);
            this.groupBox4.Controls.Add(this.checkBox6);
            this.groupBox4.Controls.Add(this.checkBox7);
            this.groupBox4.Controls.Add(this.checkBox8);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox4.Location = new System.Drawing.Point(215, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(176, 94);
            this.groupBox4.TabIndex = 64;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RH";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox5.ForeColor = System.Drawing.Color.Black;
            this.checkBox5.Location = new System.Drawing.Point(18, 35);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(76, 17);
            this.checkBox5.TabIndex = 37;
            this.checkBox5.Text = "Consultar";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox6.ForeColor = System.Drawing.Color.Black;
            this.checkBox6.Location = new System.Drawing.Point(104, 35);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(57, 17);
            this.checkBox6.TabIndex = 38;
            this.checkBox6.Text = "Salvar";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox7.ForeColor = System.Drawing.Color.Black;
            this.checkBox7.Location = new System.Drawing.Point(18, 64);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(61, 17);
            this.checkBox7.TabIndex = 39;
            this.checkBox7.Text = "Alterar";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox8.ForeColor = System.Drawing.Color.Black;
            this.checkBox8.Location = new System.Drawing.Point(104, 64);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(72, 17);
            this.checkBox8.TabIndex = 40;
            this.checkBox8.Text = "Remover";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // chkConsultarRH
            // 
            this.chkConsultarRH.AutoSize = true;
            this.chkConsultarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarRH.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarRH.Location = new System.Drawing.Point(12, 35);
            this.chkConsultarRH.Name = "chkConsultarRH";
            this.chkConsultarRH.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarRH.TabIndex = 0;
            this.chkConsultarRH.Text = "Consultar";
            this.chkConsultarRH.UseVisualStyleBackColor = true;
            // 
            // chkSalvarRH
            // 
            this.chkSalvarRH.AutoSize = true;
            this.chkSalvarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarRH.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarRH.Location = new System.Drawing.Point(113, 36);
            this.chkSalvarRH.Name = "chkSalvarRH";
            this.chkSalvarRH.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarRH.TabIndex = 1;
            this.chkSalvarRH.Text = "Salvar";
            this.chkSalvarRH.UseVisualStyleBackColor = true;
            // 
            // chkAlterarRH
            // 
            this.chkAlterarRH.AutoSize = true;
            this.chkAlterarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarRH.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarRH.Location = new System.Drawing.Point(12, 64);
            this.chkAlterarRH.Name = "chkAlterarRH";
            this.chkAlterarRH.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarRH.TabIndex = 2;
            this.chkAlterarRH.Text = "Alterar";
            this.chkAlterarRH.UseVisualStyleBackColor = true;
            // 
            // chkRemoverRH
            // 
            this.chkRemoverRH.AutoSize = true;
            this.chkRemoverRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverRH.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverRH.Location = new System.Drawing.Point(98, 64);
            this.chkRemoverRH.Name = "chkRemoverRH";
            this.chkRemoverRH.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverRH.TabIndex = 3;
            this.chkRemoverRH.Text = "Remover";
            this.chkRemoverRH.UseVisualStyleBackColor = true;
            // 
            // gpbFuncionario
            // 
            this.gpbFuncionario.Controls.Add(this.chkConsultarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkSalvarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkAlterarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkRemoverFuncionario);
            this.gpbFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.gpbFuncionario.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbFuncionario.Location = new System.Drawing.Point(200, 105);
            this.gpbFuncionario.Name = "gpbFuncionario";
            this.gpbFuncionario.Size = new System.Drawing.Size(193, 100);
            this.gpbFuncionario.TabIndex = 2;
            this.gpbFuncionario.TabStop = false;
            this.gpbFuncionario.Text = "Funcionário ";
            this.gpbFuncionario.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbVendas_Paint);
            // 
            // chkConsultarFuncionario
            // 
            this.chkConsultarFuncionario.AutoSize = true;
            this.chkConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFuncionario.Location = new System.Drawing.Point(12, 35);
            this.chkConsultarFuncionario.Name = "chkConsultarFuncionario";
            this.chkConsultarFuncionario.Size = new System.Drawing.Size(100, 22);
            this.chkConsultarFuncionario.TabIndex = 0;
            this.chkConsultarFuncionario.Text = "Consultar";
            this.chkConsultarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFuncionario
            // 
            this.chkSalvarFuncionario.AutoSize = true;
            this.chkSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFuncionario.Location = new System.Drawing.Point(112, 35);
            this.chkSalvarFuncionario.Name = "chkSalvarFuncionario";
            this.chkSalvarFuncionario.Size = new System.Drawing.Size(74, 22);
            this.chkSalvarFuncionario.TabIndex = 1;
            this.chkSalvarFuncionario.Text = "Salvar";
            this.chkSalvarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(12, 64);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(76, 22);
            this.chkAlterarFuncionario.TabIndex = 2;
            this.chkAlterarFuncionario.Text = "Alterar";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFuncionario
            // 
            this.chkRemoverFuncionario.AutoSize = true;
            this.chkRemoverFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFuncionario.Location = new System.Drawing.Point(95, 64);
            this.chkRemoverFuncionario.Name = "chkRemoverFuncionario";
            this.chkRemoverFuncionario.Size = new System.Drawing.Size(95, 22);
            this.chkRemoverFuncionario.TabIndex = 3;
            this.chkRemoverFuncionario.Text = "Remover";
            this.chkRemoverFuncionario.UseVisualStyleBackColor = true;
            // 
            // tbConteudo
            // 
            this.tbConteudo.Controls.Add(this.tpFuncionario);
            this.tbConteudo.Controls.Add(this.tpPermissao);
            this.tbConteudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbConteudo.Location = new System.Drawing.Point(3, 3);
            this.tbConteudo.Name = "tbConteudo";
            this.tbConteudo.SelectedIndex = 0;
            this.tbConteudo.Size = new System.Drawing.Size(604, 437);
            this.tbConteudo.TabIndex = 0;
            // 
            // tpFuncionario
            // 
            this.tpFuncionario.Controls.Add(this.gpbDadosPessoais);
            this.tpFuncionario.Controls.Add(this.gpbSalario);
            this.tpFuncionario.Controls.Add(this.gpbEndereco);
            this.tpFuncionario.Location = new System.Drawing.Point(4, 25);
            this.tpFuncionario.Name = "tpFuncionario";
            this.tpFuncionario.Padding = new System.Windows.Forms.Padding(3);
            this.tpFuncionario.Size = new System.Drawing.Size(596, 408);
            this.tpFuncionario.TabIndex = 0;
            this.tpFuncionario.Text = "Funcionário";
            this.tpFuncionario.UseVisualStyleBackColor = true;
            // 
            // gpbDadosPessoais
            // 
            this.gpbDadosPessoais.Controls.Add(this.txtCelular);
            this.gpbDadosPessoais.Controls.Add(this.lblCelular);
            this.gpbDadosPessoais.Controls.Add(this.txtTelefone);
            this.gpbDadosPessoais.Controls.Add(this.lblTelefone);
            this.gpbDadosPessoais.Controls.Add(this.btnProcurar);
            this.gpbDadosPessoais.Controls.Add(this.imgFuncionario);
            this.gpbDadosPessoais.Controls.Add(this.dtpNascimento);
            this.gpbDadosPessoais.Controls.Add(this.txtRG);
            this.gpbDadosPessoais.Controls.Add(this.chkM);
            this.gpbDadosPessoais.Controls.Add(this.chkF);
            this.gpbDadosPessoais.Controls.Add(this.txtCPF);
            this.gpbDadosPessoais.Controls.Add(this.txtNome);
            this.gpbDadosPessoais.Controls.Add(this.lblSexo);
            this.gpbDadosPessoais.Controls.Add(this.lblDataNascimento);
            this.gpbDadosPessoais.Controls.Add(this.lblRG);
            this.gpbDadosPessoais.Controls.Add(this.lblCPF);
            this.gpbDadosPessoais.Controls.Add(this.lblNome);
            this.gpbDadosPessoais.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosPessoais.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbDadosPessoais.Location = new System.Drawing.Point(17, -1);
            this.gpbDadosPessoais.Name = "gpbDadosPessoais";
            this.gpbDadosPessoais.Size = new System.Drawing.Size(380, 207);
            this.gpbDadosPessoais.TabIndex = 0;
            this.gpbDadosPessoais.TabStop = false;
            this.gpbDadosPessoais.Text = "Dados Pessoais ";
            this.gpbDadosPessoais.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbDadosPessoais_Paint);
            // 
            // txtCelular
            // 
            this.txtCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.ForeColor = System.Drawing.Color.Black;
            this.txtCelular.Location = new System.Drawing.Point(165, 128);
            this.txtCelular.Mask = "(99) 99999-9999";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(103, 22);
            this.txtCelular.TabIndex = 5;
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.ForeColor = System.Drawing.Color.Black;
            this.lblCelular.Location = new System.Drawing.Point(162, 110);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(66, 18);
            this.lblCelular.TabIndex = 29;
            this.lblCelular.Text = "Celular:";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.Location = new System.Drawing.Point(16, 128);
            this.txtTelefone.Mask = "(99) 99999-9999";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(105, 22);
            this.txtTelefone.TabIndex = 4;
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.ForeColor = System.Drawing.Color.Black;
            this.lblTelefone.Location = new System.Drawing.Point(12, 110);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(78, 18);
            this.lblTelefone.TabIndex = 29;
            this.lblTelefone.Text = "Telefone:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcurar.ForeColor = System.Drawing.Color.Black;
            this.btnProcurar.Location = new System.Drawing.Point(287, 172);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(84, 23);
            this.btnProcurar.TabIndex = 9;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProcurar.UseVisualStyleBackColor = true;
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // imgFuncionario
            // 
            this.imgFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgFuncionario.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Pessoas;
            this.imgFuncionario.Location = new System.Drawing.Point(287, 30);
            this.imgFuncionario.Name = "imgFuncionario";
            this.imgFuncionario.Size = new System.Drawing.Size(84, 131);
            this.imgFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFuncionario.TabIndex = 12;
            this.imgFuncionario.TabStop = false;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNascimento.Location = new System.Drawing.Point(16, 172);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(107, 22);
            this.dtpNascimento.TabIndex = 6;
            // 
            // txtRG
            // 
            this.txtRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRG.ForeColor = System.Drawing.Color.Black;
            this.txtRG.Location = new System.Drawing.Point(165, 84);
            this.txtRG.Mask = "99,999,999-9";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(103, 22);
            this.txtRG.TabIndex = 3;
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkM.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkM.ForeColor = System.Drawing.Color.Black;
            this.chkM.Location = new System.Drawing.Point(199, 172);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(40, 22);
            this.chkM.TabIndex = 8;
            this.chkM.TabStop = true;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkF.ForeColor = System.Drawing.Color.Black;
            this.chkF.Location = new System.Drawing.Point(165, 172);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(36, 22);
            this.chkF.TabIndex = 7;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // txtCPF
            // 
            this.txtCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.ForeColor = System.Drawing.Color.Black;
            this.txtCPF.Location = new System.Drawing.Point(16, 84);
            this.txtCPF.Mask = "000000000/00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(108, 22);
            this.txtCPF.TabIndex = 2;
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(16, 40);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(252, 22);
            this.txtNome.TabIndex = 1;
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.ForeColor = System.Drawing.Color.Black;
            this.lblSexo.Location = new System.Drawing.Point(162, 154);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(51, 18);
            this.lblSexo.TabIndex = 4;
            this.lblSexo.Text = "Sexo:";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.AutoSize = true;
            this.lblDataNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataNascimento.ForeColor = System.Drawing.Color.Black;
            this.lblDataNascimento.Location = new System.Drawing.Point(12, 154);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.Size = new System.Drawing.Size(166, 18);
            this.lblDataNascimento.TabIndex = 1;
            this.lblDataNascimento.Text = "Data de Nascimento:";
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG.ForeColor = System.Drawing.Color.Black;
            this.lblRG.Location = new System.Drawing.Point(162, 66);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(38, 18);
            this.lblRG.TabIndex = 1;
            this.lblRG.Text = "RG:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.Black;
            this.lblCPF.Location = new System.Drawing.Point(12, 66);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(46, 18);
            this.lblCPF.TabIndex = 1;
            this.lblCPF.Text = "CPF:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.Black;
            this.lblNome.Location = new System.Drawing.Point(12, 22);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(58, 18);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome:";
            // 
            // gpbSalario
            // 
            this.gpbSalario.Controls.Add(this.nudConvenio);
            this.gpbSalario.Controls.Add(this.nudVA);
            this.gpbSalario.Controls.Add(this.nudVT);
            this.gpbSalario.Controls.Add(this.nudVR);
            this.gpbSalario.Controls.Add(this.lblVA);
            this.gpbSalario.Controls.Add(this.nudSalario);
            this.gpbSalario.Controls.Add(this.lblCifrao5);
            this.gpbSalario.Controls.Add(this.lblCifrao4);
            this.gpbSalario.Controls.Add(this.lblCifrao3);
            this.gpbSalario.Controls.Add(this.lblCifrao2);
            this.gpbSalario.Controls.Add(this.lblCifrao);
            this.gpbSalario.Controls.Add(this.lblVT);
            this.gpbSalario.Controls.Add(this.lblConvenio);
            this.gpbSalario.Controls.Add(this.lblSalario);
            this.gpbSalario.Controls.Add(this.lblVR);
            this.gpbSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbSalario.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbSalario.Location = new System.Drawing.Point(403, -1);
            this.gpbSalario.Name = "gpbSalario";
            this.gpbSalario.Size = new System.Drawing.Size(179, 350);
            this.gpbSalario.TabIndex = 2;
            this.gpbSalario.TabStop = false;
            this.gpbSalario.Text = "Base Salarial ";
            this.gpbSalario.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbDadosPessoais_Paint);
            // 
            // nudConvenio
            // 
            this.nudConvenio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudConvenio.ForeColor = System.Drawing.Color.Black;
            this.nudConvenio.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudConvenio.Location = new System.Drawing.Point(52, 298);
            this.nudConvenio.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(121, 20);
            this.nudConvenio.TabIndex = 4;
            this.nudConvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVA
            // 
            this.nudVA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVA.ForeColor = System.Drawing.Color.Black;
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(52, 182);
            this.nudVA.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(121, 20);
            this.nudVA.TabIndex = 2;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVT
            // 
            this.nudVT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVT.DecimalPlaces = 2;
            this.nudVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVT.ForeColor = System.Drawing.Color.Black;
            this.nudVT.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVT.Location = new System.Drawing.Point(52, 124);
            this.nudVT.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVT.Name = "nudVT";
            this.nudVT.Size = new System.Drawing.Size(121, 20);
            this.nudVT.TabIndex = 1;
            this.nudVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVR
            // 
            this.nudVR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVR.ForeColor = System.Drawing.Color.Black;
            this.nudVR.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVR.Location = new System.Drawing.Point(52, 240);
            this.nudVR.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(121, 20);
            this.nudVR.TabIndex = 3;
            this.nudVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVA
            // 
            this.lblVA.AutoSize = true;
            this.lblVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVA.ForeColor = System.Drawing.Color.Black;
            this.lblVA.Location = new System.Drawing.Point(6, 162);
            this.lblVA.Name = "lblVA";
            this.lblVA.Size = new System.Drawing.Size(33, 18);
            this.lblVA.TabIndex = 39;
            this.lblVA.Text = "VA:";
            // 
            // nudSalario
            // 
            this.nudSalario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudSalario.ForeColor = System.Drawing.Color.Black;
            this.nudSalario.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalario.Location = new System.Drawing.Point(52, 66);
            this.nudSalario.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(121, 20);
            this.nudSalario.TabIndex = 0;
            this.nudSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCifrao5
            // 
            this.lblCifrao5.AutoSize = true;
            this.lblCifrao5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCifrao5.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCifrao5.Location = new System.Drawing.Point(23, 298);
            this.lblCifrao5.Name = "lblCifrao5";
            this.lblCifrao5.Size = new System.Drawing.Size(34, 18);
            this.lblCifrao5.TabIndex = 39;
            this.lblCifrao5.Text = "R$:";
            // 
            // lblCifrao4
            // 
            this.lblCifrao4.AutoSize = true;
            this.lblCifrao4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCifrao4.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCifrao4.Location = new System.Drawing.Point(23, 240);
            this.lblCifrao4.Name = "lblCifrao4";
            this.lblCifrao4.Size = new System.Drawing.Size(34, 18);
            this.lblCifrao4.TabIndex = 39;
            this.lblCifrao4.Text = "R$:";
            // 
            // lblCifrao3
            // 
            this.lblCifrao3.AutoSize = true;
            this.lblCifrao3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCifrao3.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCifrao3.Location = new System.Drawing.Point(23, 182);
            this.lblCifrao3.Name = "lblCifrao3";
            this.lblCifrao3.Size = new System.Drawing.Size(34, 18);
            this.lblCifrao3.TabIndex = 39;
            this.lblCifrao3.Text = "R$:";
            // 
            // lblCifrao2
            // 
            this.lblCifrao2.AutoSize = true;
            this.lblCifrao2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCifrao2.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCifrao2.Location = new System.Drawing.Point(23, 124);
            this.lblCifrao2.Name = "lblCifrao2";
            this.lblCifrao2.Size = new System.Drawing.Size(34, 18);
            this.lblCifrao2.TabIndex = 39;
            this.lblCifrao2.Text = "R$:";
            // 
            // lblCifrao
            // 
            this.lblCifrao.AutoSize = true;
            this.lblCifrao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCifrao.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCifrao.Location = new System.Drawing.Point(23, 66);
            this.lblCifrao.Name = "lblCifrao";
            this.lblCifrao.Size = new System.Drawing.Size(34, 18);
            this.lblCifrao.TabIndex = 39;
            this.lblCifrao.Text = "R$:";
            // 
            // lblVT
            // 
            this.lblVT.AutoSize = true;
            this.lblVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVT.ForeColor = System.Drawing.Color.Black;
            this.lblVT.Location = new System.Drawing.Point(7, 104);
            this.lblVT.Name = "lblVT";
            this.lblVT.Size = new System.Drawing.Size(33, 18);
            this.lblVT.TabIndex = 39;
            this.lblVT.Text = "VT:";
            // 
            // lblConvenio
            // 
            this.lblConvenio.AutoSize = true;
            this.lblConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConvenio.ForeColor = System.Drawing.Color.Black;
            this.lblConvenio.Location = new System.Drawing.Point(6, 278);
            this.lblConvenio.Name = "lblConvenio";
            this.lblConvenio.Size = new System.Drawing.Size(84, 18);
            this.lblConvenio.TabIndex = 39;
            this.lblConvenio.Text = "Convênio:";
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalario.ForeColor = System.Drawing.Color.Black;
            this.lblSalario.Location = new System.Drawing.Point(6, 46);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.Size = new System.Drawing.Size(66, 18);
            this.lblSalario.TabIndex = 39;
            this.lblSalario.Text = "Salário:";
            // 
            // lblVR
            // 
            this.lblVR.AutoSize = true;
            this.lblVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVR.ForeColor = System.Drawing.Color.Black;
            this.lblVR.Location = new System.Drawing.Point(6, 220);
            this.lblVR.Name = "lblVR";
            this.lblVR.Size = new System.Drawing.Size(35, 18);
            this.lblVR.TabIndex = 39;
            this.lblVR.Text = "VR:";
            // 
            // gpbEndereco
            // 
            this.gpbEndereco.Controls.Add(this.txtCidade);
            this.gpbEndereco.Controls.Add(this.imagePesquisa);
            this.gpbEndereco.Controls.Add(this.cboUF);
            this.gpbEndereco.Controls.Add(this.lblCidade);
            this.gpbEndereco.Controls.Add(this.nudNumero);
            this.gpbEndereco.Controls.Add(this.txtCEP);
            this.gpbEndereco.Controls.Add(this.lblNumero);
            this.gpbEndereco.Controls.Add(this.lblUF);
            this.gpbEndereco.Controls.Add(this.txtComplemento);
            this.gpbEndereco.Controls.Add(this.lblCompemento);
            this.gpbEndereco.Controls.Add(this.lblBairro);
            this.gpbEndereco.Controls.Add(this.txtEndereco);
            this.gpbEndereco.Controls.Add(this.lblCEP);
            this.gpbEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEndereco.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbEndereco.Location = new System.Drawing.Point(17, 206);
            this.gpbEndereco.Name = "gpbEndereco";
            this.gpbEndereco.Size = new System.Drawing.Size(380, 196);
            this.gpbEndereco.TabIndex = 1;
            this.gpbEndereco.TabStop = false;
            this.gpbEndereco.Text = "Endereço ";
            this.gpbEndereco.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbDadosPessoais_Paint);
            // 
            // txtCidade
            // 
            this.txtCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.ForeColor = System.Drawing.Color.Black;
            this.txtCidade.Location = new System.Drawing.Point(179, 165);
            this.txtCidade.MaxLength = 10000;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(140, 22);
            this.txtCidade.TabIndex = 5;
            // 
            // imagePesquisa
            // 
            this.imagePesquisa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imagePesquisa.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Alternative_Search;
            this.imagePesquisa.Location = new System.Drawing.Point(159, 37);
            this.imagePesquisa.Name = "imagePesquisa";
            this.imagePesquisa.Size = new System.Drawing.Size(23, 23);
            this.imagePesquisa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagePesquisa.TabIndex = 57;
            this.imagePesquisa.TabStop = false;
            // 
            // cboUF
            // 
            this.cboUF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboUF.ForeColor = System.Drawing.Color.Black;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(14, 163);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(140, 24);
            this.cboUF.TabIndex = 4;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblCidade.ForeColor = System.Drawing.Color.Black;
            this.lblCidade.Location = new System.Drawing.Point(175, 147);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(65, 18);
            this.lblCidade.TabIndex = 56;
            this.lblCidade.Text = "Cidade:";
            // 
            // nudNumero
            // 
            this.nudNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudNumero.ForeColor = System.Drawing.Color.Black;
            this.nudNumero.Location = new System.Drawing.Point(180, 122);
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(140, 22);
            this.nudNumero.TabIndex = 3;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.ForeColor = System.Drawing.Color.Black;
            this.txtCEP.Location = new System.Drawing.Point(15, 37);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(140, 22);
            this.txtCEP.TabIndex = 0;
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblNumero.ForeColor = System.Drawing.Color.Black;
            this.lblNumero.Location = new System.Drawing.Point(176, 104);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(37, 18);
            this.lblNumero.TabIndex = 53;
            this.lblNumero.Text = "N° :";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblUF.ForeColor = System.Drawing.Color.Black;
            this.lblUF.Location = new System.Drawing.Point(10, 146);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(35, 18);
            this.lblUF.TabIndex = 52;
            this.lblUF.Text = "UF:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(15, 121);
            this.txtComplemento.MaxLength = 10000;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(140, 22);
            this.txtComplemento.TabIndex = 2;
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblCompemento.ForeColor = System.Drawing.Color.Black;
            this.lblCompemento.Location = new System.Drawing.Point(10, 104);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(118, 18);
            this.lblCompemento.TabIndex = 51;
            this.lblCompemento.Text = "Complemento:";
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblBairro.ForeColor = System.Drawing.Color.Black;
            this.lblBairro.Location = new System.Drawing.Point(10, 61);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(85, 18);
            this.lblBairro.TabIndex = 46;
            this.lblBairro.Text = "Endereço:";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(15, 81);
            this.txtEndereco.MaxLength = 70;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(332, 22);
            this.txtEndereco.TabIndex = 1;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblCEP.ForeColor = System.Drawing.Color.Black;
            this.lblCEP.Location = new System.Drawing.Point(10, 20);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(47, 18);
            this.lblCEP.TabIndex = 48;
            this.lblCEP.Text = "CEP:";
            // 
            // frmSalvar_Funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbConteudo);
            this.Name = "frmSalvar_Funcionario";
            this.Size = new System.Drawing.Size(607, 438);
            this.tpPermissao.ResumeLayout(false);
            this.gpbVendas.ResumeLayout(false);
            this.gpbVendas.PerformLayout();
            this.gpbAcesso.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gpbEstoque.ResumeLayout(false);
            this.gpbEstoque.PerformLayout();
            this.gpbCompras.ResumeLayout(false);
            this.gpbCompras.PerformLayout();
            this.gpbFinanceiro.ResumeLayout(false);
            this.gpbFinanceiro.PerformLayout();
            this.gpbRH.ResumeLayout(false);
            this.gpbRH.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gpbFuncionario.ResumeLayout(false);
            this.gpbFuncionario.PerformLayout();
            this.tbConteudo.ResumeLayout(false);
            this.tpFuncionario.ResumeLayout(false);
            this.gpbDadosPessoais.ResumeLayout(false);
            this.gpbDadosPessoais.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).EndInit();
            this.gpbSalario.ResumeLayout(false);
            this.gpbSalario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.gpbEndereco.ResumeLayout(false);
            this.gpbEndereco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePesquisa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tpPermissao;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox gpbVendas;
        private System.Windows.Forms.CheckBox chkConsultarVendas;
        private System.Windows.Forms.CheckBox chkSalvarVendas;
        private System.Windows.Forms.CheckBox chkAlterarVendas;
        private System.Windows.Forms.CheckBox chkRemoverVendas;
        private System.Windows.Forms.GroupBox gpbAcesso;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.GroupBox gpbEstoque;
        private System.Windows.Forms.CheckBox chkConsultarEstoque;
        private System.Windows.Forms.CheckBox chkSalvarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarEstoque;
        private System.Windows.Forms.CheckBox chkRemoverEstoque;
        private System.Windows.Forms.GroupBox gpbCompras;
        private System.Windows.Forms.CheckBox chkConsultarCompras;
        private System.Windows.Forms.CheckBox chkSalvarCompras;
        private System.Windows.Forms.CheckBox chkAlterarCompras;
        private System.Windows.Forms.CheckBox chkRemoverCompras;
        private System.Windows.Forms.GroupBox gpbFinanceiro;
        private System.Windows.Forms.CheckBox chkConsultarFinanceiro;
        private System.Windows.Forms.CheckBox chkSalvarFinanceiro;
        private System.Windows.Forms.CheckBox chkAlterarFinanceiro;
        private System.Windows.Forms.CheckBox chkRemoverFinanceiro;
        private System.Windows.Forms.GroupBox gpbRH;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox chkConsultarRH;
        private System.Windows.Forms.CheckBox chkSalvarRH;
        private System.Windows.Forms.CheckBox chkAlterarRH;
        private System.Windows.Forms.CheckBox chkRemoverRH;
        private System.Windows.Forms.GroupBox gpbFuncionario;
        private System.Windows.Forms.CheckBox chkConsultarFuncionario;
        private System.Windows.Forms.CheckBox chkSalvarFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkRemoverFuncionario;
        private System.Windows.Forms.TabControl tbConteudo;
        private System.Windows.Forms.TabPage tpFuncionario;
        private System.Windows.Forms.GroupBox gpbDadosPessoais;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.PictureBox imgFuncionario;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblDataNascimento;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.GroupBox gpbSalario;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.NumericUpDown nudVT;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.Label lblVA;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Label lblCifrao5;
        private System.Windows.Forms.Label lblCifrao4;
        private System.Windows.Forms.Label lblCifrao3;
        private System.Windows.Forms.Label lblCifrao2;
        private System.Windows.Forms.Label lblCifrao;
        private System.Windows.Forms.Label lblVT;
        private System.Windows.Forms.Label lblConvenio;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.Label lblVR;
        private System.Windows.Forms.GroupBox gpbEndereco;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.PictureBox imagePesquisa;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.NumericUpDown nudNumero;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label lblCompemento;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblCEP;
    }
}
