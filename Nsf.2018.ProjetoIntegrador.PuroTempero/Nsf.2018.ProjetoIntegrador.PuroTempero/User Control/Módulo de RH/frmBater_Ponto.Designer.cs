﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH
{
    partial class frmBater_Ponto
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtxtOBS = new System.Windows.Forms.RichTextBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.gpbInformações = new System.Windows.Forms.GroupBox();
            this.gpbBaterPonto = new System.Windows.Forms.GroupBox();
            this.lblFim = new System.Windows.Forms.Label();
            this.dtpFim = new System.Windows.Forms.DateTimePicker();
            this.lblAlmocoVolta = new System.Windows.Forms.Label();
            this.dtpAlmocoVolta = new System.Windows.Forms.DateTimePicker();
            this.lblAlmoçoSaida = new System.Windows.Forms.Label();
            this.dtpAlmocoSaida = new System.Windows.Forms.DateTimePicker();
            this.dtpComeço = new System.Windows.Forms.DateTimePicker();
            this.lblEntrada = new System.Windows.Forms.Label();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.dtpSaida = new System.Windows.Forms.DateTimePicker();
            this.dtpEntrada = new System.Windows.Forms.DateTimePicker();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.btnBaterPonto = new System.Windows.Forms.Button();
            this.lblHorario = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.gpbHorário = new System.Windows.Forms.GroupBox();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.imgFuncionario = new System.Windows.Forms.PictureBox();
            this.gpbInformações.SuspendLayout();
            this.gpbBaterPonto.SuspendLayout();
            this.gpbHorário.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // rtxtOBS
            // 
            this.rtxtOBS.Enabled = false;
            this.rtxtOBS.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtOBS.Location = new System.Drawing.Point(6, 129);
            this.rtxtOBS.Name = "rtxtOBS";
            this.rtxtOBS.Size = new System.Drawing.Size(155, 105);
            this.rtxtOBS.TabIndex = 30;
            this.rtxtOBS.Text = "A partir dessa tela, é possível gerenciar a quantidade de hora que os funcionário" +
    "s trabalham dentro da empresa. ";
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(4, 112);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(36, 16);
            this.lblOBS.TabIndex = 28;
            this.lblOBS.Text = "OBS:";
            // 
            // gpbInformações
            // 
            this.gpbInformações.Controls.Add(this.rtxtOBS);
            this.gpbInformações.Controls.Add(this.imgImagem);
            this.gpbInformações.Controls.Add(this.lblOBS);
            this.gpbInformações.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformações.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformações.Location = new System.Drawing.Point(429, 152);
            this.gpbInformações.Name = "gpbInformações";
            this.gpbInformações.Size = new System.Drawing.Size(167, 241);
            this.gpbInformações.TabIndex = 60;
            this.gpbInformações.TabStop = false;
            this.gpbInformações.Text = "Informações ";
            this.gpbInformações.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbHorário_Paint);
            // 
            // gpbBaterPonto
            // 
            this.gpbBaterPonto.Controls.Add(this.lblFim);
            this.gpbBaterPonto.Controls.Add(this.dtpFim);
            this.gpbBaterPonto.Controls.Add(this.lblAlmocoVolta);
            this.gpbBaterPonto.Controls.Add(this.dtpAlmocoVolta);
            this.gpbBaterPonto.Controls.Add(this.lblAlmoçoSaida);
            this.gpbBaterPonto.Controls.Add(this.dtpAlmocoSaida);
            this.gpbBaterPonto.Controls.Add(this.dtpComeço);
            this.gpbBaterPonto.Controls.Add(this.lblEntrada);
            this.gpbBaterPonto.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbBaterPonto.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbBaterPonto.Location = new System.Drawing.Point(9, 152);
            this.gpbBaterPonto.Name = "gpbBaterPonto";
            this.gpbBaterPonto.Size = new System.Drawing.Size(414, 241);
            this.gpbBaterPonto.TabIndex = 59;
            this.gpbBaterPonto.TabStop = false;
            this.gpbBaterPonto.Text = "Bater Ponto ";
            this.gpbBaterPonto.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbHorário_Paint);
            // 
            // lblFim
            // 
            this.lblFim.AutoSize = true;
            this.lblFim.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblFim.ForeColor = System.Drawing.Color.Black;
            this.lblFim.Location = new System.Drawing.Point(15, 174);
            this.lblFim.Name = "lblFim";
            this.lblFim.Size = new System.Drawing.Size(50, 19);
            this.lblFim.TabIndex = 53;
            this.lblFim.Text = "Saída:";
            // 
            // dtpFim
            // 
            this.dtpFim.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFim.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFim.Location = new System.Drawing.Point(85, 167);
            this.dtpFim.Name = "dtpFim";
            this.dtpFim.Size = new System.Drawing.Size(71, 26);
            this.dtpFim.TabIndex = 52;
            this.dtpFim.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // lblAlmocoVolta
            // 
            this.lblAlmocoVolta.AutoSize = true;
            this.lblAlmocoVolta.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblAlmocoVolta.ForeColor = System.Drawing.Color.Black;
            this.lblAlmocoVolta.Location = new System.Drawing.Point(220, 110);
            this.lblAlmocoVolta.Name = "lblAlmocoVolta";
            this.lblAlmocoVolta.Size = new System.Drawing.Size(106, 19);
            this.lblAlmocoVolta.TabIndex = 51;
            this.lblAlmocoVolta.Text = "Almoço/Volta:";
            // 
            // dtpAlmocoVolta
            // 
            this.dtpAlmocoVolta.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAlmocoVolta.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpAlmocoVolta.Location = new System.Drawing.Point(328, 103);
            this.dtpAlmocoVolta.Name = "dtpAlmocoVolta";
            this.dtpAlmocoVolta.Size = new System.Drawing.Size(71, 26);
            this.dtpAlmocoVolta.TabIndex = 50;
            this.dtpAlmocoVolta.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // lblAlmoçoSaida
            // 
            this.lblAlmoçoSaida.AutoSize = true;
            this.lblAlmoçoSaida.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblAlmoçoSaida.ForeColor = System.Drawing.Color.Black;
            this.lblAlmoçoSaida.Location = new System.Drawing.Point(15, 110);
            this.lblAlmoçoSaida.Name = "lblAlmoçoSaida";
            this.lblAlmoçoSaida.Size = new System.Drawing.Size(108, 19);
            this.lblAlmoçoSaida.TabIndex = 49;
            this.lblAlmoçoSaida.Text = "Almoço/Saída:";
            // 
            // dtpAlmocoSaida
            // 
            this.dtpAlmocoSaida.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAlmocoSaida.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpAlmocoSaida.Location = new System.Drawing.Point(124, 103);
            this.dtpAlmocoSaida.Name = "dtpAlmocoSaida";
            this.dtpAlmocoSaida.Size = new System.Drawing.Size(71, 26);
            this.dtpAlmocoSaida.TabIndex = 48;
            this.dtpAlmocoSaida.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // dtpComeço
            // 
            this.dtpComeço.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpComeço.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpComeço.Location = new System.Drawing.Point(85, 41);
            this.dtpComeço.Name = "dtpComeço";
            this.dtpComeço.Size = new System.Drawing.Size(71, 26);
            this.dtpComeço.TabIndex = 46;
            this.dtpComeço.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // lblEntrada
            // 
            this.lblEntrada.AutoSize = true;
            this.lblEntrada.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblEntrada.ForeColor = System.Drawing.Color.Black;
            this.lblEntrada.Location = new System.Drawing.Point(15, 48);
            this.lblEntrada.Name = "lblEntrada";
            this.lblEntrada.Size = new System.Drawing.Size(64, 19);
            this.lblEntrada.TabIndex = 47;
            this.lblEntrada.Text = "Entrada:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcurar.ForeColor = System.Drawing.Color.Black;
            this.btnProcurar.Location = new System.Drawing.Point(441, 109);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(141, 27);
            this.btnProcurar.TabIndex = 38;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.UseVisualStyleBackColor = true;
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuncionario.Location = new System.Drawing.Point(116, 35);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(308, 26);
            this.txtFuncionario.TabIndex = 36;
            // 
            // dtpSaida
            // 
            this.dtpSaida.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSaida.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpSaida.Location = new System.Drawing.Point(212, 94);
            this.dtpSaida.Name = "dtpSaida";
            this.dtpSaida.Size = new System.Drawing.Size(71, 26);
            this.dtpSaida.TabIndex = 28;
            this.dtpSaida.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // dtpEntrada
            // 
            this.dtpEntrada.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEntrada.Location = new System.Drawing.Point(88, 94);
            this.dtpEntrada.Name = "dtpEntrada";
            this.dtpEntrada.Size = new System.Drawing.Size(71, 26);
            this.dtpEntrada.TabIndex = 28;
            this.dtpEntrada.Value = new System.DateTime(2018, 9, 6, 0, 0, 0, 0);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(391, 400);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(100, 28);
            this.btnVoltar.TabIndex = 58;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnBaterPonto
            // 
            this.btnBaterPonto.BackColor = System.Drawing.Color.DarkGreen;
            this.btnBaterPonto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBaterPonto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaterPonto.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaterPonto.ForeColor = System.Drawing.Color.White;
            this.btnBaterPonto.Location = new System.Drawing.Point(497, 400);
            this.btnBaterPonto.Name = "btnBaterPonto";
            this.btnBaterPonto.Size = new System.Drawing.Size(99, 28);
            this.btnBaterPonto.TabIndex = 57;
            this.btnBaterPonto.Text = "Bater Ponto";
            this.btnBaterPonto.UseVisualStyleBackColor = false;
            // 
            // lblHorario
            // 
            this.lblHorario.AutoSize = true;
            this.lblHorario.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblHorario.ForeColor = System.Drawing.Color.Black;
            this.lblHorario.Location = new System.Drawing.Point(16, 94);
            this.lblHorario.Name = "lblHorario";
            this.lblHorario.Size = new System.Drawing.Size(66, 19);
            this.lblHorario.TabIndex = 25;
            this.lblHorario.Text = "Horário:";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.Black;
            this.lblHora.Location = new System.Drawing.Point(176, 104);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(20, 13);
            this.lblHora.TabIndex = 25;
            this.lblHora.Text = "ÀS";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblFuncionario.ForeColor = System.Drawing.Color.Black;
            this.lblFuncionario.Location = new System.Drawing.Point(18, 37);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(91, 19);
            this.lblFuncionario.TabIndex = 17;
            this.lblFuncionario.Text = "Funcionário:";
            // 
            // gpbHorário
            // 
            this.gpbHorário.Controls.Add(this.btnProcurar);
            this.gpbHorário.Controls.Add(this.imgFuncionario);
            this.gpbHorário.Controls.Add(this.txtFuncionario);
            this.gpbHorário.Controls.Add(this.dtpSaida);
            this.gpbHorário.Controls.Add(this.dtpEntrada);
            this.gpbHorário.Controls.Add(this.lblHorario);
            this.gpbHorário.Controls.Add(this.lblHora);
            this.gpbHorário.Controls.Add(this.lblFuncionario);
            this.gpbHorário.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbHorário.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbHorário.Location = new System.Drawing.Point(8, 0);
            this.gpbHorário.Name = "gpbHorário";
            this.gpbHorário.Size = new System.Drawing.Size(588, 146);
            this.gpbHorário.TabIndex = 56;
            this.gpbHorário.TabStop = false;
            this.gpbHorário.Text = "Horário ";
            this.gpbHorário.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbHorário_Paint);
            // 
            // imgImagem
            // 
            this.imgImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Dinner;
            this.imgImagem.Location = new System.Drawing.Point(7, 23);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(154, 77);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // imgFuncionario
            // 
            this.imgFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgFuncionario.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Employer;
            this.imgFuncionario.Location = new System.Drawing.Point(441, 25);
            this.imgFuncionario.Name = "imgFuncionario";
            this.imgFuncionario.Size = new System.Drawing.Size(141, 78);
            this.imgFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFuncionario.TabIndex = 37;
            this.imgFuncionario.TabStop = false;
            // 
            // frmBater_Ponto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbInformações);
            this.Controls.Add(this.gpbBaterPonto);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.btnBaterPonto);
            this.Controls.Add(this.gpbHorário);
            this.Name = "frmBater_Ponto";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbInformações.ResumeLayout(false);
            this.gpbInformações.PerformLayout();
            this.gpbBaterPonto.ResumeLayout(false);
            this.gpbBaterPonto.PerformLayout();
            this.gpbHorário.ResumeLayout(false);
            this.gpbHorário.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtxtOBS;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.PictureBox imgFuncionario;
        private System.Windows.Forms.GroupBox gpbInformações;
        private System.Windows.Forms.GroupBox gpbBaterPonto;
        private System.Windows.Forms.Label lblFim;
        private System.Windows.Forms.DateTimePicker dtpFim;
        private System.Windows.Forms.Label lblAlmocoVolta;
        private System.Windows.Forms.DateTimePicker dtpAlmocoVolta;
        private System.Windows.Forms.Label lblAlmoçoSaida;
        private System.Windows.Forms.DateTimePicker dtpAlmocoSaida;
        private System.Windows.Forms.DateTimePicker dtpComeço;
        private System.Windows.Forms.Label lblEntrada;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.DateTimePicker dtpSaida;
        private System.Windows.Forms.DateTimePicker dtpEntrada;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.Button btnBaterPonto;
        private System.Windows.Forms.Label lblHorario;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.GroupBox gpbHorário;
    }
}
