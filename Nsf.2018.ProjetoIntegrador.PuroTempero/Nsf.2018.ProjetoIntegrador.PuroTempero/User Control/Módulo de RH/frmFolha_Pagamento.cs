﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH
{
    public partial class frmFolha_Pagamento : UserControl
    {
        public frmFolha_Pagamento()
        {
            InitializeComponent();
        }

        private void gpbFolhaPagamento_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }


        private void rdnNaoA_CheckedChanged_1(object sender, EventArgs e)
        {
            nudVA.Enabled = false;
        }

        private void rdnSimA_CheckedChanged_1(object sender, EventArgs e)
        {
            nudVA.Enabled = true;
        }

        private void rdnNaoS_CheckedChanged(object sender, EventArgs e)
        {
            nudPlanoSaude.Enabled = false;
        }

        private void rdnSimS_CheckedChanged_1(object sender, EventArgs e)
        {
            nudPlanoSaude.Enabled = true;
        }

        //declaro as variáveis de escopo
        int diastrabalhados = 0;
        decimal salariohora, horastrabalhadas, valealimentacao, planosaude, horastotal, salario, salariototal;

        private void imgPesquisa_Click(object sender, EventArgs e)
        {

        }

        private decimal SalarioHora ()
        {
            //Pego a quantidade de dias trabalhados
            diastrabalhados = Convert.ToInt32(nudDiasTrabalhados.Value);

            //Pego quanto o funcionário ganha por hora
            salariohora = nudSalarioHora.Value;

            //Pego a quantidade de horas trabalhadas
            horastrabalhadas = Convert.ToDecimal(txtHorasTrabalhadas.Text);

            //Multiplicado os dias trabalhados com as horas trabalhadas para saber o valor total.
            horastotal = diastrabalhados * horastrabalhadas;

            //De acordo com o total de horas multiplicado com suas horas de trabalho, eu declararei o seu sálario
            salario = horastotal * horastrabalhadas;

            //Caso tenho plano de saúde
            if (rdnSimS.Checked == true)
            {
                planosaude = nudPlanoSaude.Value;
            }
            //Caso tenho Vale alimentação
            if (rdnSimA.Checked == true)
            {
                valealimentacao = nudVA.Value;
            }
            salariototal = salario - valealimentacao - planosaude;
            return salariototal;
        }
        private void Relatorio ()
        {
            //Variáveis "escopo" 
            string nome, 
                 //cargo
                   diastrabalhados, 
                   valealimentacao = string.Empty, 
                   planosaude = string.Empty, 
                   horastotal, 
                   salariototal,
                   conteudo;

            //Pegando os valores dos controles
            //cboCargo = cboCargo.SelectedItem;
            nome = txtNomeFuncionario.Text;
            diastrabalhados = nudDiasTrabalhados.Value.ToString();

            //Passando o valor em texto da RadioButton
            if (rdnSimS.Checked == true)
            {
                planosaude = "Sim";
            }
            else
            {
                planosaude = "Não";
            }

            //Novamente, o mesmo processo!
            if (rdnSimA.Checked == true)
            {
                valealimentacao = "Sim";
            }
            else
            {
                valealimentacao = "Não";
            }

            //Continue pegando o valor dos controles
            horastotal = txtHorasTrabalhadas.Text;

            //Chamo o Método SalarioHora
            salariototal = SalarioHora().ToString();

            conteudo = @"O funcionário " + nome + "\n" +
                        "Trabalhou durante " + diastrabalhados + " dias, " +
                        " por " + horastotal + " horas " + "\n" +
                        "Recendo o equivalente à R$: " + salariototal + ",00 " + "\n\n" +
                        "Desse valor, possui os descontos de: " + "\n" +
                        "Plano de sáude: " + planosaude + "\n" +
                        "Vale Alimentação: " + valealimentacao;

            //Passo o valor para o método PDF;
            PDF pdf = new PDF();
            pdf.Pdf(conteudo);

        }
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal salario = SalarioHora();
            lblSalario.Text = salario.ToString();
        }

        private void btnGerarPagamento_Click(object sender, EventArgs e)
        {
            Relatorio();
        }
    }
}
