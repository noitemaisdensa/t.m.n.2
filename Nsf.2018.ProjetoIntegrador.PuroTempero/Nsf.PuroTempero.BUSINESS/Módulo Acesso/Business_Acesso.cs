﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Database;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Business
{
    public class Business_Acesso
    {
        public int Salvar(DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            return db.Salvar(dto);
        }
        public void Alterar(DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            db.Alterar(dto);

        }
        public void Remover(DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            db.Remover(dto);

        }
        public List<DTO_Acesso> Consultar(DTO_Acesso dto)
        {
            Database_Acesso db = new Database_Acesso();
            return db.Consultar(dto);

        }

    }
}
