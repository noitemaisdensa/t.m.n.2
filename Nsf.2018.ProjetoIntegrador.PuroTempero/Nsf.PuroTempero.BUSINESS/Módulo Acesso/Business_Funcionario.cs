﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Validadores;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Módulo_de_Acesso.Funcionário;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário
{
    public class Business_Funcionario
    {
        Database_Funcionario db = new Database_Funcionario();

        public int Salvar(DTO_Funcionario dto)
        {
            return db.Salvar(dto);
        }
        public void Alterar(DTO_Funcionario dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Funcionario dto)
        {
            db.Remover(dto);

        }
        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            List<DTO_Funcionario> consult = db.Consultar(dto);
            return consult;
        }
    }
}
