﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH
{
    public class Business_RH
    {
        Database_RH db = new Database_RH();

        public int Salvar (DTO_RH dto)
        {
            return db.Salvar(dto);
        }
        public void Remover (DTO_RH dto)
        {
            db.Remover(dto);
        }
        public void Alterar (DTO_RH dto)
        {
            db.Alterar(dto);
        }
        public List<DTO_RH> Listar ()
        {
            List<DTO_RH> list = db.Listar();
            return list;
        }
    }
}
