﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente
{
    public class Business_Cliente
    {
        Database_Cliente db = new Database_Cliente();

        public int Salvar(DTO_Cliente dto)
        {
            return db.Salvar(dto);
        }

        public void Alterar(DTO_Cliente dto)
        {
            db.Alterar(dto);
        }

        public void Remover(DTO_Cliente dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Cliente> Listar()
        {
            List<DTO_Cliente> list = db.Listar();
            return list;
        }
        public List<DTO_Cliente> Consultar(DTO_Cliente dt)
        {
            List<DTO_Cliente> consult = db.Consultar(dt);
            return consult;
        }
    }
}
