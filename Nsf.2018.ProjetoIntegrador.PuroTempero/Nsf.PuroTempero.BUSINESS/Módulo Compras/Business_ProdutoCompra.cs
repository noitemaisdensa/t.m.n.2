﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business
{
    public class Business_ProdutoCompra
    {
        //Método de escopo
        Database_ProdutoCompra db = new Database_ProdutoCompra();

        //Método de Salvar
        public int Salvar (DTO_ProdutoCompra dto)
        {
            return db.Salvar(dto);
        }

        //Método de Remover
        public void Remover (int id)
        {
            db.Remover(id);
        }

        //Método de Alterar
        public void Alterar (DTO_ProdutoCompra dto)
        {
            db.Alterar(dto);
        }

        //Método de Listar (Sem filtro)
        public List<DTO_ProdutoCompra> Listar()
        {
            List<DTO_ProdutoCompra> list = db.Listar();
            return list;
        }

        //Método de Consultar (Com filtro)
        public List<DTO_ProdutoCompra> Consultar(DTO_ProdutoCompra dto)
        {
            List<DTO_ProdutoCompra> list = db.Consultar(dto);
            return list;
        }
    }
}
