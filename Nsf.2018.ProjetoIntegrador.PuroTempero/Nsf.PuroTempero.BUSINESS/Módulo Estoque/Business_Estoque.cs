﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    public class Business_Estoque
    {
        //Objeto utilizado como escopo
        Database_Estoque db = new Database_Estoque();

        //MÉTODO DE SALVAR
        //OBS: Esse método vai receber uma lista de DTO, pois como podem aparecer vários itens em um 
        //MÉTODO DE LISTAR, precisaremos salvar todos os valores que irão aparecer.
        public void Salvar(List<DTO_Estoque> dto)
        {
            //TRADUÇÃO: Para cada valor encontrado no DTO, passa-se o valor para a variável chamada ITEM
            //E pegamos o valor passado por parâmetro chamada DTO
            foreach (DTO_Estoque item in dto)
            {
                //Chamo o MÉTODO DE SALVAR passando o valor da variável na repetição
                db.Salvar(item);
            }
        }

        //Método de remover PADRÃO FIFA!
        public void Remover (int id)
        {
            db.Remover(id);
        }
        
        //Método de listar PADRÃO FIFA!
        public List<DTO_Estoque> Listar ()
        {
            List<DTO_Estoque> list = db.Listar();
            return list;
        }
    }
}
