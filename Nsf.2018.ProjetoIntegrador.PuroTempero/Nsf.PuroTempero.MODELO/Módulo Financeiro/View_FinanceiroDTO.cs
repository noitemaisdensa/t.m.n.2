﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_Financeiro
{
    public class VIEW_FinanceiroDTO
    {
        public DateTime Data { get; set; }
        public decimal Total { get; set; }
        public string Operacao { get; set; }
    }
}
