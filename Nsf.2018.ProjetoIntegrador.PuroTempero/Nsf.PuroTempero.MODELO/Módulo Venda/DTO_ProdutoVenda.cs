﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos
{
    public class DTO_ProdutoVenda
    {
        public int ID { get; set; }
        public string Produto { get; set; }
        public decimal Preco { get; set; }
        public string  Imagem { get; set; }
    }
}
