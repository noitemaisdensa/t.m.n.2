﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    public class DTO_Estoque
    {
        public int ID { get; set; }
        public int ID_Produto { get; set; }
        public int ID_Pedido { get; set; }
    }
}

