﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.MODELO.Módulo_Acesso
{
    public class DTO_Departamento
    {
        public int ID_Departamento { get; set; }
        public string Departamento { get; set; }
    }
}
