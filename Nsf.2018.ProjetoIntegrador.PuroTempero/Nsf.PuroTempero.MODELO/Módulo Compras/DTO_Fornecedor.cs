﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras
{
    public class DTO_Fornecedor
    {
        public int ID { get; set; }
        public string Razao_Social { get; set; }
        public string CNPJ { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public string Complemento { get; set; }
        public decimal Numero_Casa { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
    }
}
