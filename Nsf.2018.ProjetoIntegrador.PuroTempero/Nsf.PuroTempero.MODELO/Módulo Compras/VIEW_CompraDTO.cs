﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.MODELO.Módulo_Compras
{
    public class VIEW_CompraDTO
    {
        public int ID { get; set; }
        public DateTime Data { get; set; }
        public string Fornecedor { get; set; }
        public int Quantidade { get; set; }
        public decimal Total { get; set; }
    }
}
