﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem
{
    public static class ImagemPlugin
    {
            //Esta primeira classe converte a foto em um diretório
            public static string ConverterParaString(Image imagem)
            {
                //Instância a função responsável por transformar os valores passados em Bytes
                MemoryStream memoria = new MemoryStream();

                //Pega o valor passado como parâmetro, depois chama a função que irá transformar essa imagem em diretório
                imagem.Save(memoria, imagem.RawFormat);

                //Transforma o valor em uma lista
                byte[] imagebytes = memoria.ToArray();

                //Transforma o valor em string
                string ImagemEmTexto = Convert.ToBase64String(imagebytes);
                return ImagemEmTexto;
            }

            //Já essa, pega o o diretório em string e transforma em uma imagem.
            public static Image ConverterParaImagem(string ImagemEmTexto)
            {
                //Pega o valor do paramêtro, e o transformo novamente em base binária.
                byte[] bytes = Convert.FromBase64String(ImagemEmTexto);

                //E depois, com o valor em binário, eu o transformo em imagem novamente.
                Image imagem = Image.FromStream(new MemoryStream(bytes));
                return imagem;
            }
    }
}
