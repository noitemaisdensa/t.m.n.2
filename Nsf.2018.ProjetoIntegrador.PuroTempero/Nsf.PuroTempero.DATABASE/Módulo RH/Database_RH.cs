﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH
{
    public class Database_RH
    {
        public int Salvar(DTO_RH dto)
        {
            string script =
            @"INSERT INTO tb_ponto  
            (
                dt_data, 
                hr_entrada, 
                hr_almoco_saida, 
                hr_almoco_retorno, 
                hr_saida, 
                id_funcionario
            )
            VALUES 
            (   
                @dt_data, 
                @hr_entrada, 
                @hr_almoco_saida, 
                @hr_almoco_retorno, 
                @hr_saida, 
                @id_funcionario
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_data", dto.Data));
            parms.Add(new MySqlParameter("hr_entrada", dto.hr_entrada));
            parms.Add(new MySqlParameter("hr_almoco_saida", dto.hr_almoco_saida));
            parms.Add(new MySqlParameter("hr_almoco_retorno", dto.hr_almoco_retorno));
            parms.Add(new MySqlParameter("hr_saida", dto.hr_saida));
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_RH dto)
        {
            string script = @"UPDATE tb_ponto 
                                 SET dt_data             =@dt_data,
                                     hr_entrada          =@hr_entrada,
                                     hr_almoco_saida     =@hr_almoco_saida,
                                     hr_almoco_retorno   =@hr_almoco_retorno,
                                     hr_saida            =@hr_saida,
                                     id_funcionario      =@id_funcionario
                              WHERE  id_ponto            =@id_ponto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_data", dto.Data));
            parms.Add(new MySqlParameter("hr_entrada", dto.hr_entrada));
            parms.Add(new MySqlParameter("hr_almoco_saida", dto.hr_almoco_saida));
            parms.Add(new MySqlParameter("hr_almoco_retorno", dto.hr_almoco_retorno));
            parms.Add(new MySqlParameter("hr_saida", dto.hr_saida));
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));
            parms.Add(new MySqlParameter("id_ponto", dto.id_ponto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(DTO_RH dto)
        {
            string script = @"DELETE FROM tb_ponto
                                    WHERE id_ponto =@id_ponto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_ponto", dto.id_ponto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_RH> Listar()
        {
            string script = @"SELECR * FROM tb_ponto";

            Database dvb = new Database();
            MySqlDataReader reader = dvb.ExecuteSelectScript(script, null);
            List<DTO_RH> lista = new List<DTO_RH>();

            while (reader.Read())
            {
                DTO_RH dto = new DTO_RH();
                dto.id_ponto = reader.GetInt32("id_ponto");
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.Data = reader.GetDateTime("dt_data");
                dto.hr_almoco_retorno = reader.GetString("hr_almoco_retorno");
                dto.hr_almoco_saida = reader.GetString("hr_almoco_saida");
                dto.hr_entrada = reader.GetString("hr_entrada");
                dto.hr_saida = reader.GetString("hr_saida");

                lista.Add(dto);
            }

            reader.Close();
            return lista;
        }
    }
}
