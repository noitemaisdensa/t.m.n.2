﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_RH.Folha_de_Pagamento
{
    public class Database_FolhaPagamento
    {
        public int Salvar(DTO_FolhaPagamento dto)
        {
            string script =
             @"INSERT INTO tb_folha_pagamento
             (
                 nr_horas_trabalhadas, 
                 vl_horas_extras, 
                 vl_fgts, 
                 vl_inss, 
                 vl_ir, 
                 vl_dependente, 
                 vl_bruto, 
                 vl_liquido, 
                 id_funcionario
             )
             VALUES
             (
                 @nr_horas_trabalhadas,
                 @vl_horas_extras, 
                 @vl_fgts, 
                 @vl_inss, 
                 @vl_dependente, 
                 @vl_bruto, 
                 @vl_liquido, 
                 @id_funcionario
             )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_horas_trabalhadas", dto.Horas_Trabalhadas));
            parms.Add(new MySqlParameter("vl_horas_extras", dto.Valor_Horas_Extras));
            parms.Add(new MySqlParameter("vl_fgts", dto.FGTS));
            parms.Add(new MySqlParameter("vl_inss", dto.INSS));
            parms.Add(new MySqlParameter("vl_ir", dto.IR));
            parms.Add(new MySqlParameter("vl_dependente", dto.Dependente));
            parms.Add(new MySqlParameter("vl_bruto", dto.Bruto));
            parms.Add(new MySqlParameter("vl_liquido", dto.Liquido));
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            DB.Base.Database db = new Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
      

    }
}
