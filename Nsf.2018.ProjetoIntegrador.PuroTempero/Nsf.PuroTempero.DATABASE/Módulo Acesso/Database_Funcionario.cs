﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Módulo_de_Acesso.Funcionário
{
    public class Database_Funcionario
    {
        DB.Base.Database db = new DB.Base.Database();

        public int Salvar(DTO_Funcionario dto)
        {
            string script =
            @"INSERT INTO tb_funcionario
            (
                nm_funcionario, 
                ds_rg,
                ds_cpf, 
                dt_nascimento, 
                tp_sexo, 
                vl_vale_transporte, 
                vl_vale_refeicao, 
                vl_vale_alimentacao, 
                vl_vale_convenio, 
                vl_salario, 
                img_foto, 
                ds_usuario, 
                ds_senha, 
                nm_uf, 
                nm_cidade, 
                nm_endereco, 
                nr_residencia, 
                ds_cep, 
                ds_complemento, 
                ds_email, 
                ds_telefone, 
                ds_celular
            )
            VALUES 
            (
                @nm_funcionario, 
                @ds_rg,
                @ds_cpf,
                @dt_nascimento, 
                @tp_sexo, 
                @vl_vale_transporte, 
                @vl_vale_refeicao, 
                @vl_vale_alimentacao, 
                @vl_vale_convenio,
                @vl_salario, 
                @img_foto, 
                @ds_usuario, 
                @ds_senha,
                @nm_uf, 
                @nm_cidade, 
                @nm_endereco, 
                @nr_residencia, 
                @ds_cep, 
                @ds_complemento, 
                @ds_email, 
                @ds_telefone, 
                @ds_celular
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.Transporte));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.Alimentacao));
            parms.Add(new MySqlParameter("vl_vale_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_uf", dto.UF));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereço));
            parms.Add(new MySqlParameter("nr_residencia", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));

            DB.Base.Database db = new DB.Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_Funcionario dto)
        {
            string script =
                @"UPDATE tb_funcionario 
                     SET nm_funcionario         = @nm_funcionario,
                         ds_rg                  = @ds_rg,
                         ds_cpf                 = @ds_cpf,
                         dt_nascimento          = @dt_nascimento,
                         tp_sexo                = @tp_sexo,
                         vl_vale_transporte     = @vl_vale_transporte,
                         vl_vale_refeicao       = @vl_vale_refeicao,
                         vl_vale_alimentacao    = @vl_vale_alimentacao,
                         vl_vale_convenio       = @vl_vale_convenio,
                         vl_salario             = @vl_salario,
                         img_foto               = @img_foto,
                         ds_usuario             = @ds_usuario,
                         ds_senha               = @ds_senha,
                         nm_uf                  = @nm_uf,
                         nm_cidade              = @nm_cidade,
                         nm_endereco            = @nm_endereco,
                         nr_residencia          = @nr_residencia,
                         ds_cep                 = @ds_cep,
                         ds_complemento         = @ds_complemento,
                         ds_email               = @ds_email,
                         ds_telefone            = @ds_telefone,
                         ds_celular             = @ds_celular
                   WHERE id_funcionario         = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.Transporte));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.Alimentacao));
            parms.Add(new MySqlParameter("vl_vale_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_uf", dto.UF));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereço));
            parms.Add(new MySqlParameter("nr_residencia", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(DTO_Funcionario dto)
        {
            string script = @"DELETE FROM tb_funcionario 
                                    WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
                                      WHERE nm_funcionario like @nm_funcionario 
                                        AND ds_rg like @ds_rg
                                        AND ds_cpf like @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));
            parms.Add(new MySqlParameter("ds_rg", "%" + dto.RG + "%"));
            parms.Add(new MySqlParameter("ds_cpf", "%" + dto.CPF + "%"));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            while(reader.Read())
            {
                DTO_Funcionario dt = new DTO_Funcionario();
                dt.Id = reader.GetInt32("id_funcionario");
                dt.Nome = reader.GetString("nm_funcionario");
                dt.CPF = reader.GetString("ds_cpf");
                dt.RG = reader.GetString("ds_rg");
                dt.Telefone = reader.GetString("ds_telefone");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Endereço = reader.GetString("nm_endereco");
                dt.UF = reader.GetString("nm_uf");
                lista.Add(dt);

            }
            reader.Close();
            return lista;


        }

        //Servindo aqui como meio de descriptografar as senhas.
        public bool VerificarSenha(string senha)
        {
            string script = @"SELECT * FROM tb_funcionario
                                       WHERE ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_senha", senha));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
