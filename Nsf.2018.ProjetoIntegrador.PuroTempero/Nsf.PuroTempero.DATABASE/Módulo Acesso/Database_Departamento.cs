﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf.PuroTempero.MODELO.Módulo_Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.BUSINESS.Módulo_Acesso
{
    public class Database_Departamento
    {
        public int PesquisarID(DTO_Departamento dto)
        {
            string script =
                @"SELECT * FROM tb_departamento WHERE nm_departamento = @nm_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", dto.Departamento));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            int id = 0;
            while (reader.Read())
            {
                DTO_Departamento dt = new DTO_Departamento();
                dt.ID_Departamento = reader.GetInt32("id_departamento");
                dt.Departamento = reader.GetString("nm_departamento");
                id = dt.ID_Departamento;

            }
            return id;
        }
    }
}
