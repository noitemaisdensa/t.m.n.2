﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.DATABASE.Módulo_Venda
{
    public class Database_ProdutoVenda
    {
        public int Salvar (DTO_ProdutoVenda dto)
        {
            string script =
            @"INSERT INTO tb_produto_venda
            (
                nm_produto,
                vl_preco,
                img_imagem
            )
            VALUES
            (
               @nm_produto,
               @vl_preco,
               @img_imagem
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("img_imagem", dto.Imagem));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Remover (int id)
        {
            string script = @"DELETE FROM tb_produto_venda
                                    WHERE id_produto_venda = @id_produto_venda";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto_venda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public void Alterar (DTO_ProdutoVenda dto)
        {
            string script = @"UPDATE tb_produto_venda
                                 SET nm_produto         =@nm_produto,
                                     vl_preco           =@vl_preco,
                                     img_imagem         =@img_imagem
                              WHERE  id_produto_venda   =@id_produto_venda";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto_venda", dto.ID));
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("img_imagem", dto.Imagem));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public List<DTO_ProdutoVenda> Listar ()
        {
            string script = @"SELECT * FROM tb_produto_venda";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_ProdutoVenda> fora = new List<DTO_ProdutoVenda>();

            while (reader.Read())
            {
                DTO_ProdutoVenda dentro = new DTO_ProdutoVenda();
                dentro.ID = reader.GetInt32("id_produto_venda");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Imagem = reader.GetString("img_imagem");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
        public List<DTO_ProdutoVenda> Consultar(DTO_ProdutoVenda dto)
        {
            string script = @"SELECT * FROM tb_produto_venda
                                      WHERE nm_produto LIKE @nm_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", "%" + dto.Produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_ProdutoVenda> fora = new List<DTO_ProdutoVenda>();

            while (reader.Read())
            {
                DTO_ProdutoVenda dentro = new DTO_ProdutoVenda();
                dentro.ID = reader.GetInt32("id_produto_venda");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Imagem = reader.GetString("img_imagem");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
