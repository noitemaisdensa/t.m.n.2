﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Database
{
    public class Database_ProdutoCompra
    {
        //Método de escopo
        Base.Database db = new Base.Database();

        //Método de Salvar
        public int Salvar(DTO_ProdutoCompra dto)
        {
            string script =
            @"INSERT INTO tb_produto_compra
            (
                id_fornecedor,
                nm_produto,
                vl_preco,
                nm_marca,
                img_imagem
            )
            VALUES
            (
               @id_fornecedor,
               @nm_produto,
               @vl_preco,
               @nm_marca,
               @img_imagem
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("nm_marca", dto.Marca));
            parm.Add(new MySqlParameter("img_imagem", dto.Imagem));

            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        //Método de Remover
        public void Remover(int dto)
        {
            string script = @"DELETE FROM tb_produto_compra
                                    WHERE id_produto = @id_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto", dto));

            db.ExecuteInsertScript(script, parm);
        }

        //Método de Alterar
        public void Alterar(DTO_ProdutoCompra dto)
        {
            string script =
                @"UPDATE tb_produto_compra
                     SET    id_fornecedor   =@id_fornecedor,
                            nm_produto      =@nm_produto,
                            vl_preco        =@vl_preco,
                            nm_marca        =@nm_marca,
                            img_imagem      =@img_imagem
                  WHERE     id_produto      =@id_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto", dto.ID));
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("nm_marca", dto.Marca));
            parm.Add(new MySqlParameter("img_imagem", dto.Imagem));

            db.ExecuteInsertScript(script, parm);
        }

        //Método de listar (Sem filtro). Este método trás todos os registros da tabela.
        public List<DTO_ProdutoCompra> Listar()
        {
            string script = "SELECT * FROM tb_produto_compra";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_ProdutoCompra> fora = new List<DTO_ProdutoCompra>();

            while (reader.Read())
            {
                DTO_ProdutoCompra dentro = new DTO_ProdutoCompra();
                dentro.ID = reader.GetInt32("id_produto");
                dentro.ID_Fornecedor = reader.GetInt32("id_fornecedor");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Marca = reader.GetString("nm_marca");
                dentro.Imagem = reader.GetString("img_imagem");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        //Método de consultar (Com filtro). Este método trás todos os valores correspondentre a um determinado produto
        public List<DTO_ProdutoCompra> Consultar (DTO_ProdutoCompra dto)
        {
            string script = @"SELECT * FROM tb_produto_compra
                                      WHERE nm_produto LIKE @nm_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", "%" + dto.Produto + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_ProdutoCompra> fora = new List<DTO_ProdutoCompra>();

            while (reader.Read())
            {
                DTO_ProdutoCompra dentro = new DTO_ProdutoCompra();
                dentro.ID = reader.GetInt32("id_produto");
                dentro.ID_Fornecedor = reader.GetInt32("id_fornecedor");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Marca = reader.GetString("nm_marca");
                dentro.Imagem = reader.GetString("img_imagem");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
