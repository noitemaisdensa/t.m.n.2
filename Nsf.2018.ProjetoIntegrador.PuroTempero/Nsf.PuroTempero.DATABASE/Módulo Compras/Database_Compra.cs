﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Database
{
    public class Database_Compra
    {
        public int Salvar (DTO_Compra dto)
        {
            string script =
            @"INSERT INTO tb_compra
            (
                id_fornecedor,
                dt_compra
            )
            VALUES
            (
                @id_fornecedor,
                @dt_compra
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("dt_compra", dto.Data_Compra));

            Base.Database db = new Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        
    }
}
